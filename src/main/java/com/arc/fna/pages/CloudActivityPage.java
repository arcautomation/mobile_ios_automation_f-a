package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.regexp.RE;
import org.bridj.cpp.std.list;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.arc.fna.utils.ARCFNAAppUtils;
import com.arcautoframe.utils.Log;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class CloudActivityPage {

	WebDriver cloudDriver;
	
	@FindBy(xpath = ".//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span")
	List<WebElement> firstFolderInFolderTrees;
	
	@FindBy(xpath = ".//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span")
	WebElement firstFolderInFolderTree;
	
	@FindBy(xpath = ".//*[@id='navBtnLink']/div/ul/li[8]/a")
	WebElement removeFolderInDropDown;
	
	@FindBy(css = "#lftpnlMore")
	WebElement collectionLevelMorebutton;
	
	@FindBy(css = ".selectedTreeRow")
	WebElement firstFolder1;
	
	@FindBy(xpath = ".//*[@id='listGrid']/div[2]/table/tbody/tr[2]/td[3]/a")
	WebElement collectionSearched;
	
	
	@FindBy(css = ".btn.btn-primary.center-block")
	List<WebElement> gotItBanners;
	
	@FindBy(css = ".btn.btn-primary.center-block")
	WebElement gotItBannerBtn;

	@FindBy(css = ".icon.icon-edit.icon-orange")
	WebElement btnCollectionEdit;

	@FindBy(xpath = ".//*[@id='popup']/div/div/div[3]/div[2]/input[1]")
	WebElement moveFolderok;

	@FindBy(xpath = ".//*[@id='popupTree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span")
	WebElement targetFolder;

	@FindBy(css = "#liMoveFile1>a")
	WebElement moveFile;

	@FindBy(css = "#liCopyFile1>a")
	WebElement copyFile;

	@FindBy(xpath = ".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[1]/img")
	WebElement firstFileCheckBox;

	@FindBy(xpath = ".//*[@id='popup']/div/div/div[3]/div[2]/input[1]")
	WebElement moveFileOKbtn;

	@FindBy(xpath = ".//*[@id='popupTree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span")
	WebElement moveToFolder;

	@FindBy(xpath = ".//*[@id='liMoveFile1']/a")
	WebElement moveFileBtn;

	@FindBy(xpath = ".//*[@id='Button1']")
	WebElement moreBtnFile;

	@FindBy(xpath = ".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[1]/img")
	WebElement fileSelection;

	// @FindBy(xpath =
	// ".//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span")
	@FindBy(xpath = ".//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span")
	List<WebElement> folderSelectionTreeList;

	@FindBy(xpath = ".//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span")
	WebElement folderColumnTree;

	@FindBy(xpath = ".//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span")
	WebElement firstFolder;

	@FindBy(xpath = ".//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span")
	WebElement testAutomationProject;

	@FindBy(css = ".ev_material>td>img")
	WebElement fileCheckBox;

	@FindBy(css = "#txtSearchValue")
	WebElement fileSearchTextBox;

	@FindBy(css = "#ProjectFiles>a")
	WebElement btnDocuments;

	@FindBy(css = "#btnNewFolderSave")
	WebElement folderSave;

	@FindBy(css = "#txtFolderName")
	WebElement folderNameTextbox;

	@FindBy(css = "#btnCreateFolder")
	WebElement createFolder;

	@FindBy(css = "#btnRemoveProject")
	WebElement removeProject;

	@FindBy(css = ".ev_material>td>img")
	List<WebElement> collectionCheckboxes;

	@FindBy(css = ".ev_material>td>img")
	WebElement collectionCheckbox;

	@FindBy(css = "#btnSearch")
	WebElement btnSearchIcon;

	@FindBy(css = "#txtSearchValue")
	WebElement collectionSearchBox;

	@FindBy(css = "#btnSave")
	WebElement btnCollectionSave;

	@FindBy(css = "#btnNewProject")
	WebElement btnAddCollections;

	@FindBy(css = "#Collections>a")
	WebElement btnCollections;

	@FindBy(css = ".icon.icon-include-latest-documents-folder.icon-3x.media-object")
	List<WebElement> folderIcons;

	@FindBy(css = ".icon.icon-include-latest-documents-folder.icon-3x.media-object")
	WebElement folderIcon;

	@FindBy(css = "#txtSearchKeyword")
	WebElement folderSearchTxtBox;

	@FindBy(css = "#btnSearch")
	WebElement folderBtnSearch;

	@FindBy(css = ".icon.icon-project-dashboard.icon-lg.pw-icon-white.pulse.animated")
	WebElement btnProjectDashboard;

	@FindBy(css = ".icon.icon-more-option")
	WebElement collectionMoreicon;

	@FindBy(css = ".icon.icon-project.icon-4x")
	List<WebElement> listCollectionIcon;

	@FindBy(css = ".icon.icon-project.icon-4x")
	WebElement collectionIcon;

	@FindBy(css = ".ev_material>td>img")
	WebElement collectionSelectionCheckBox;

	@FindBy(css = ".ev_material>td>img")
	List<WebElement> collectionSelectionCheckBoxs;

	@FindBy(xpath = ".//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a/i")
	List<WebElement> editBtnList;

	@FindBy(xpath = ".//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a")
	List<WebElement> collectionlist;

	@FindBy(xpath = ".//*[@id='liCopyFile1']/a")
	WebElement btnCopyFile;

	@FindBy(xpath = ".//*[@id='popupTree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[5]/td[2]/table/tbody/tr/td[4]/span")
	WebElement btnMoveFolderSelection;

	@FindBy(xpath = ".//*[@id='liMoveFile1']/a")
	WebElement btnMoveFile;

	@FindBy(xpath = ".//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[6]/td[2]/table/tbody/tr/td[4]/span")
	WebElement btnFolder6;

	@FindBy(xpath = ".//*[@id='imageViewer']")
	WebElement imageViewer;

	@FindBy(xpath = ".//*[@id='lineToolMenuBtn']")
	WebElement btnLineMenu;

	@FindBy(xpath = ".//*[@id='arrowCreate']/a/i")
	WebElement btnLineMenuArrow;

	@FindBy(css = "#liDeleteFile1>a")
	WebElement btnDeleteFile;

	@FindBy(css = "#Button1")
	WebElement fileMoreButton;

	@FindBy(css = ".ev_material>td>img")
	WebElement fileSelect;

	@FindBy(css = ".ev_material>td>img")
	WebElement fileSelect1;

	@FindBy(css = ".ev_material>td>img")
	List<WebElement> fileSelects1;

	@FindBy(css = ".ev_material>td>img")
	List<WebElement> filesSelect;

	@FindBy(xpath = ".//*[@id='popup']/div/div/div[3]/div[2]/input[1]")
	WebElement btnPopupOk;

	@FindBy(id = "multipartUploadBtn")
	WebElement btnUpload;

	@FindBy(xpath = ".//*[@id='btnSelectFiles']")
	WebElement btnSelectFile;

	@FindBy(xpath = ".//*[@id='dvAppletUploadEnabled']/div/div[1]/div/label[2]")
	WebElement btnNewCopy;

	@FindBy(xpath = ".//*[@id='btnUploadFile']")
	WebElement btnUploadFile;

	@FindBy(xpath = ".//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[5]/td[2]/table/tbody/tr/td[4]/span")
	WebElement staticFolder1;

	@FindBy(xpath = ".//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span")
	WebElement staticFolder2;

	@FindBy(xpath = ".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")
	WebElement file1;

	// @FindBy(xpath =
	// ".//*[@id='mainPageContainer']/div[1]/ul[1]/li[1]/a/span[1]/img")
	@FindBy(css = ".img-circle.img-responsive.hoverZoomLink")
	WebElement btnProfile;

	@FindBy(css = ".delete-projectfolder.btnDelete")
	WebElement btnRemoveFolder;

	@FindBy(css = ".icon.icon-ellipsis-horizontal.media-object.icon-lg")
	WebElement btnMore;

	@FindBy(xpath = ".//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span")
	WebElement btnDynamicFolder;

	@FindBy(xpath = ".//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span")
	List<WebElement> btnDynamicFolders;

	@FindBy(css = "#btnFolderCreate")
	WebElement btnFolderSave;

	@FindBy(css = "#txtFolderName")
	WebElement txtFolderName;

	@FindBy(css = "#aPrjAddFolder")
	WebElement btnCreateFolder;

	@FindBy(xpath = ".//*[@id='listGrid']/div[2]/table/tbody/tr[2]/td[3]/a")
	WebElement btnStaticCollection;

	@FindBy(css = ".objbox")
	WebElement containerCollection;

	@FindBy(css = "#button-1")
	WebElement btnYesDeleteCollection;

	@FindBy(xpath = ".//*[@id='dv_6uMRngm10XFQnTXt89mCIA%3d%3d']/span/div/ul/li[8]/a")
	WebElement btnRemoveCollection;

	@FindBy(css = ".ev_material>td>img")
	// @FindBy(xpath = ".//*[@id='listGrid']/div[2]/table/tbody/tr[2]/td[1]/img")
	WebElement chkBoxcollectionSelect;

	@FindBy(css = ".ev_material>td>img")
	// @FindBy(xpath = ".//*[@id='listGrid']/div[2]/table/tbody/tr[2]/td[1]/img")
	List<WebElement> chkBoxcollectionSelects;

	@FindBy(css = "#btnSearch")
	WebElement btnSearch;

	@FindBy(css = "#txtSearchKeyword")
	WebElement txtSearchBoxCollection;

	@FindBy(id = "UserID")
	WebElement userName1;

	@FindBy(id = "Password")
	WebElement passWord1;

	@FindBy(css = "#btnLogin")
	WebElement btnLogin1;

	@FindBy(css = "#Collections > a")
	WebElement btnCollection;

	@FindBy(css = ".nav.navbar-nav.navbar-right>li>a>span")
	WebElement btnAddCollection;

	@FindBy(css = "#txtProjectName")
	WebElement txtProjectName;

	@FindBy(css = "#btnCreate")
	WebElement btnSaveCollection;

	@FindBy(id = "myFrame")
	WebElement frameMyFrame;

	@FindBy(id = "txtProjectInvitationMsg_ifr")
	WebElement frametxtProjectInvitationMsg_ifr;

	public CloudActivityPage(WebDriver cloudDriver) {
		this.cloudDriver = cloudDriver;
	}

	/**
	 * Login into F&A cloud application
	 * 
	 * @throws InterruptedException
	 */

	public void loginWeb() throws InterruptedException {
			try
			{
			ARCFNAAppUtils.waitForElement1(cloudDriver, userName1, 10);
			userName1.sendKeys(ARCFNAAppUtils.testDataReading("username"));
			Log.message("User name has been inserted.");
			ARCFNAAppUtils.waitForElement1(cloudDriver, passWord1, 5);
			passWord1.sendKeys(ARCFNAAppUtils.testDataReading("password"));
			Log.message("Password has been inserted.");
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnLogin1, 5);
			btnLogin1.click();
			Log.message("Login button is clicked");
			Thread.sleep(10000);
			Log.message("SKYSITE FNA login successful in CLOUD");
			}
			catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void loginWebStgLoad() throws InterruptedException {
		try {
			Thread.sleep(4000);
			ARCFNAAppUtils.waitForElement1(cloudDriver, userName1, 10);
			userName1.sendKeys("dassubhendu8780@gmail.com");
			ARCFNAAppUtils.waitForElement1(cloudDriver, passWord1, 5);
			passWord1.sendKeys("arcind@123");
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnLogin1, 5);
			btnLogin1.click();
			LogEntries logEntries = cloudDriver.manage().logs().get(LogType.BROWSER);
			for (LogEntry entry : logEntries) {
				Thread.sleep(10000);
				System.out.println("Exit Unit Open : " + entry.getMessage());

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Opening collection list
	 */

	public void openCollectionList() {
		Log.message("FNA Website login done");
		ARCFNAAppUtils.waitForElement1(cloudDriver, txtSearchBoxCollection, 20);
		txtSearchBoxCollection.sendKeys("Automation");
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnSearch, 10);
		btnSearch.click();
		Log.message("Search button is clicked");
		ARCFNAAppUtils.waitForElement1(cloudDriver, collectionIcon, 10);
		collectionIcon.click();
		Log.message("Collection List is opened.");
		ARCFNAAppUtils.waitTill(5000);
	}

	public void openStaticCollection() throws InterruptedException {
		// ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		// cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitForElement1(cloudDriver, collectionIcon, 20);
		collectionIcon.click();
		Thread.sleep(4000);

	}

	public void deleteDynamicFolder() throws InterruptedException {
		ARCFNAAppUtils.waitForElement1(cloudDriver, folderSearchTxtBox, 20);
		folderSearchTxtBox.sendKeys("FolderCreated");
		ARCFNAAppUtils.waitForElement1(cloudDriver, folderBtnSearch, 10);
		folderBtnSearch.click();
		ARCFNAAppUtils.waitTill(4000);

		int folCnt = folderIcons.size();

		Log.message("************************" + " " + Integer.toString(folCnt));

		if (folCnt > 0) {
			ARCFNAAppUtils.waitForElement1(cloudDriver, folderIcon, 10);
			folderIcon.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnMore, 10);
			btnMore.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnRemoveFolder, 10);
			btnRemoveFolder.click();
			for (String winHandle : cloudDriver.getWindowHandles()) {
				cloudDriver.switchTo().window(winHandle);
			}

			ARCFNAAppUtils.waitForElement1(cloudDriver, btnYesDeleteCollection, 20);
			btnYesDeleteCollection.click();
			ARCFNAAppUtils.waitTill(4000);
		} else {
			Log.message("Folder not found");
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnProjectDashboard, 20);
			btnProjectDashboard.click();
			Thread.sleep(4000);
			ARCFNAAppUtils.waitForElement1(cloudDriver, txtSearchBoxCollection, 20);
			txtSearchBoxCollection.sendKeys("Automation");
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnSearch, 10);
			btnSearch.click();
			Log.message("Search button is clicked");
			Thread.sleep(5000);
			ARCFNAAppUtils.waitForElement1(cloudDriver, collectionIcon, 20);
			collectionIcon.click();
			Thread.sleep(4000);
		}
		ARCFNAAppUtils.waitTill(4000);

	}
	
	public void deleteFirstFolder()
	{
	
		String mainwindow = cloudDriver.getWindowHandle();
		ARCFNAAppUtils.waitTill(7000);
		
		if(firstFolderInFolderTrees.size()>0)
		{
		ARCFNAAppUtils.waitTill(3000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, firstFolderInFolderTree, 20);
		firstFolderInFolderTree.click();
		ARCFNAAppUtils.waitTill(3000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, collectionLevelMorebutton, 20);
		collectionLevelMorebutton.click();
		ARCFNAAppUtils.waitForElement1(cloudDriver, removeFolderInDropDown, 20);
		removeFolderInDropDown.click();
		for (String childwindow1 : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(childwindow1);
		}
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnYesDeleteCollection, 20);
		btnYesDeleteCollection.click();
		ARCFNAAppUtils.waitTill(4000);
		cloudDriver.switchTo().window(mainwindow);
		ARCFNAAppUtils.waitTill(4000);
		}
		Log.message("Folder deleted from the opened Project, through cloud / web");
	}

	public void searchStaticCollection() throws InterruptedException {

		ARCFNAAppUtils.waitForElement1(cloudDriver, txtSearchBoxCollection, 20);
		txtSearchBoxCollection.sendKeys("Automation");
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnSearch, 10);
		btnSearch.click();
		Log.message("Search button is clicked");
		Thread.sleep(5000);
	}

	public void searchDynamicCollection() throws InterruptedException {
		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitForElement1(cloudDriver, txtSearchBoxCollection, 20);
		txtSearchBoxCollection.sendKeys("Sample Facility");
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnSearch, 10);
		btnSearch.click();
		Log.message("Search button is clicked");
		Thread.sleep(5000);
	}

	public void folderCreationDynamic() throws InterruptedException {
		Log.message("Searching for frame");
		// cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnCreateFolder, 20);
		btnCreateFolder.click();
		ARCFNAAppUtils.waitTill(2000);
		for (String winHandle : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(winHandle);
		}
		Thread.sleep(5000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, txtFolderName, 20);
		txtFolderName.sendKeys("FolderCreated");
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnFolderSave, 10);
		btnFolderSave.click();
		ARCFNAAppUtils.waitTill(4000);
	}

	public void subFolderCreationDynamic() throws InterruptedException {
		Log.message("Searching for frame");
		// cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnCreateFolder, 20);
		btnCreateFolder.click();
		ARCFNAAppUtils.waitTill(2000);
		for (String winHandle : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(winHandle);
		}
		Thread.sleep(5000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitForElement1(cloudDriver, txtFolderName, 20);
		txtFolderName.sendKeys("Dynamic Folder Testing");
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnFolderSave, 10);
		btnFolderSave.click();
		ARCFNAAppUtils.waitTill(4000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, btnCreateFolder, 20);
		btnCreateFolder.click();
		ARCFNAAppUtils.waitTill(2000);
		for (String winHandle : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(winHandle);
		}
		Thread.sleep(5000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitForElement1(cloudDriver, txtFolderName, 20);
		txtFolderName.sendKeys("Sub Dynamic Folder Testing");
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnFolderSave, 10);
		btnFolderSave.click();
		ARCFNAAppUtils.waitTill(4000);

	}

	/**
	 * Adding collection
	 */
	public void addingCollection() {
		ARCFNAAppUtils.waitTill(7000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnAddCollection, 20);
		btnAddCollection.click();
		Log.message("Add Collection button is clicked");
		ARCFNAAppUtils.waitTill(10000);
		String parentHandle = cloudDriver.getWindowHandle();
		for (String winHandle : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(winHandle);
			String winurl = cloudDriver.switchTo().window(winHandle).getCurrentUrl();
		}
		ARCFNAAppUtils.waitForElement1(cloudDriver, txtProjectName, 20);
		txtProjectName.sendKeys("Integration Test Project");
		btnSaveCollection.click();
		ARCFNAAppUtils.waitTill(5000);
		Log.message("Collection has been added.");
	}

	public void btnProfileLoadTime() {
		WebDriverWait wait = new WebDriverWait(cloudDriver, 10);
		// wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".img-circle.img-responsive.hoverZoomLink")));
		wait.until(ExpectedConditions.elementToBeClickable(btnProfile));
	}

	public void closeBrowser() {
		cloudDriver.quit();
		Log.message("Browser has been closed.");
	}

	public void openFolder() {
		ARCFNAAppUtils.waitForElement1(cloudDriver, staticFolder1, 20);
		staticFolder1.click();
		ARCFNAAppUtils.waitTill(4000);
	}

	public void openFolder2() {
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnFolder6, 20);
		btnFolder6.click();
		ARCFNAAppUtils.waitTill(2000);
	}

	public void openFolder1() {
		ARCFNAAppUtils.waitForElement1(cloudDriver, staticFolder2, 20);
		staticFolder2.click();
		ARCFNAAppUtils.waitTill(4000);
	}

	public void openFile1() {
		ARCFNAAppUtils.waitForElement1(cloudDriver, file1, 20);
		file1.click();
		ARCFNAAppUtils.waitTill(6000);

	}

	public void uploadFileInitiate() throws AWTException, IOException {
		ARCFNAAppUtils.waitTill(4000);
		String mainwindow = cloudDriver.getWindowHandle();
		ARCFNAAppUtils.waitTill(3000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitTill(5000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, btnUploadFile, 30);
		btnUploadFile.click();

		ARCFNAAppUtils.waitTill(5000);
		for (String childwindow1 : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(childwindow1);
		}

		ARCFNAAppUtils.waitTill(6000);

		btnSelectFile.click();
		ARCFNAAppUtils.waitTill(5000);

		Runtime.getRuntime().exec("osascript" + " " + "/Users/dv/Subhendu/AppleScript/Fileupload.scpt");
		ARCFNAAppUtils.waitTill(10000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, btnUpload, 30);
		btnUpload.click();
		ARCFNAAppUtils.waitTill(10000);
		cloudDriver.switchTo().window(mainwindow);
		ARCFNAAppUtils.waitTill(5000);
		
		Log.message("File uploaded successfully through Cloud / Web");

	}

	public void selectFile() throws AWTException, InterruptedException, URISyntaxException, IOException {
		ARCFNAAppUtils.waitTill(7000);
		String parentHandle = cloudDriver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(winHandle);
			ARCFNAAppUtils.waitTill(5000);// switch focus of WebDriver to the next found window handle (that's your
											// newly // opened window)
		}

		btnSelectFile.click();
		ARCFNAAppUtils.waitTill(5000);

		Runtime.getRuntime().exec("osascript" + " " + "/Users/dv/Subhendu/AppleScript/Fileupload.scpt");
		ARCFNAAppUtils.waitTill(3000);

	}

	public void uploadFile(String mainwindow) {
		String parentHandle = cloudDriver.getWindowHandle();
		Log.message(parentHandle);
		// String mainwindow = cloudDriver.getWindowHandle();
		for (String winHandle : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(parentHandle);
			ARCFNAAppUtils.waitTill(5000);// switch focus of WebDriver to the next found window handle (that's your
											// newly // opened window)
		}
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnUpload, 30);
		btnUpload.click();
		ARCFNAAppUtils.waitTill(10000);
		Log.message("000001");
		cloudDriver.switchTo().window(mainwindow);
		// cloudDriver.switchTo().defaultContent();
		ARCFNAAppUtils.waitTill(5000);
		Log.message("000002");
	}

	public void deleteFile() {

		ARCFNAAppUtils.waitTill(4000);
		String mainwindow = cloudDriver.getWindowHandle();
		ARCFNAAppUtils.waitTill(3000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitTill(6000);
		Log.message("000004");

		ARCFNAAppUtils.waitForElement1(cloudDriver, fileSearchTextBox, 20);
		fileSearchTextBox.sendKeys("Release PDF.PDF");
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnSearch, 20);
		btnSearch.click();
		ARCFNAAppUtils.waitTill(7000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, fileCheckBox, 20);
		fileCheckBox.click();
		ARCFNAAppUtils.waitForElement1(cloudDriver, fileMoreButton, 20);
		fileMoreButton.click();
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnDeleteFile, 20);
		btnDeleteFile.click();
		ARCFNAAppUtils.waitTill(4000);

		for (String childwindow1 : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(childwindow1);
		}
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnYesDeleteCollection, 20);
		btnYesDeleteCollection.click();
		ARCFNAAppUtils.waitTill(4000);

		cloudDriver.switchTo().window(mainwindow);
		ARCFNAAppUtils.waitTill(5000);
	}

	public void deleteFile1() {

		// ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		// cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitForElement1(cloudDriver, fileSelect1, 30);
		if (fileSelects1.size() > 0) {
			ARCFNAAppUtils.waitForElement1(cloudDriver, fileSelect1, 20);
			fileSelect1.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, fileMoreButton, 20);
			fileMoreButton.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnDeleteFile, 15);
			btnDeleteFile.click();
			ARCFNAAppUtils.waitTill(4000);
			for (String winHandle : cloudDriver.getWindowHandles()) {
				cloudDriver.switchTo().window(winHandle);
				ARCFNAAppUtils.waitTill(5000);// switch focus of WebDriver to the next found window handle (that's your
												// newly // opened window)
			}

			ARCFNAAppUtils.waitForElement1(cloudDriver, btnYesDeleteCollection, 20);
			btnYesDeleteCollection.click();

			ARCFNAAppUtils.waitTill(5000);
			cloudDriver.switchTo().defaultContent();
			ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
			cloudDriver.switchTo().frame(frameMyFrame);
		}
	}

	public void deleteFileAfterUpload() {
		for (String winHandle : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(winHandle);
			ARCFNAAppUtils.waitTill(5000);// switch focus of WebDriver to the next found window handle (that's your
											// newly // opened window)
		}
		// cloudDriver.switchTo().defaultContent();
		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitForElement1(cloudDriver, fileSelect, 30);
		if (filesSelect.size() > 0) {
			ARCFNAAppUtils.waitForElement1(cloudDriver, fileSelect, 20);
			fileSelect.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, fileMoreButton, 20);
			fileMoreButton.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnDeleteFile, 15);
			btnDeleteFile.click();
			ARCFNAAppUtils.waitTill(4000);
			for (String winHandle : cloudDriver.getWindowHandles()) {
				cloudDriver.switchTo().window(winHandle);
				ARCFNAAppUtils.waitTill(5000);// switch focus of WebDriver to the next found window handle (that's your
												// newly // opened window)
			}

			ARCFNAAppUtils.waitForElement1(cloudDriver, btnYesDeleteCollection, 20);
			btnYesDeleteCollection.click();

			ARCFNAAppUtils.waitTill(5000);
			cloudDriver.switchTo().defaultContent();
			ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
			cloudDriver.switchTo().frame(frameMyFrame);
		}
	}

	public void toolSelection() {
		for (String winHandle : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(winHandle);
			ARCFNAAppUtils.waitTill(5000);
		}
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnLineMenu, 20);
		btnLineMenu.click();
		ARCFNAAppUtils.waitTill(4000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnLineMenuArrow, 20);
		// btnLineMenuArrow.click();
		JavascriptExecutor executor = (JavascriptExecutor) cloudDriver;
		executor.executeScript("arguments[0].click();", btnLineMenuArrow);
		ARCFNAAppUtils.waitTill(4000);
	}

	public void fileSelectAndMove() {
		for (String winHandle : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(winHandle);
			ARCFNAAppUtils.waitTill(5000);// switch focus of WebDriver to the next found window handle (that's your
											// newly // opened window)
		}
		// cloudDriver.switchTo().defaultContent();
		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitForElement1(cloudDriver, fileSelect, 30);
		if (filesSelect.size() > 0) {
			ARCFNAAppUtils.waitForElement1(cloudDriver, fileSelect, 20);
			fileSelect.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, fileMoreButton, 20);
			fileMoreButton.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnMoveFile, 20);
			btnMoveFile.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnMoveFolderSelection, 20);
			btnMoveFolderSelection.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnPopupOk, 20);
			btnPopupOk.click();
			ARCFNAAppUtils.waitTill(5000);
		}
	}

	public void fileCopyAndPaste() {
		for (String winHandle : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(winHandle);
			ARCFNAAppUtils.waitTill(5000);// switch focus of WebDriver to the next found window handle (that's your
											// newly // opened window)
		}
		// cloudDriver.switchTo().defaultContent();
		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitForElement1(cloudDriver, fileSelect, 30);
		if (filesSelect.size() > 0) {
			ARCFNAAppUtils.waitForElement1(cloudDriver, fileSelect, 20);
			fileSelect.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, fileMoreButton, 20);
			fileMoreButton.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnCopyFile, 20);
			btnCopyFile.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnMoveFolderSelection, 20);
			btnMoveFolderSelection.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnPopupOk, 20);
			btnPopupOk.click();
			ARCFNAAppUtils.waitTill(5000);
		}
	}

	// ˆˆˆˆˆˆˆˆˆˆˆˆˆˆˆˆˆˆˆ

	public void addingCollectionNameChange() {
		ARCFNAAppUtils.waitTill(7000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnAddCollection, 20);
		btnAddCollection.click();
		Log.message("Add Collection button is clicked");
		ARCFNAAppUtils.waitTill(10000);
		String parentHandle = cloudDriver.getWindowHandle();
		for (String winHandle : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(winHandle);
			String winurl = cloudDriver.switchTo().window(winHandle).getCurrentUrl();
		}
		ARCFNAAppUtils.waitForElement1(cloudDriver, txtProjectName, 20);
		txtProjectName.sendKeys("CollectionName");
		btnSaveCollection.click();
		ARCFNAAppUtils.waitTill(5000);
		Log.message("Collection has been added.");

		for (String winHandle : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(winHandle);
			String winurl = cloudDriver.switchTo().window(winHandle).getCurrentUrl();
		}
	}

	public void deleteChangedNameCollection() throws InterruptedException {
		
		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitForElement1(cloudDriver, txtSearchBoxCollection, 20);
		txtSearchBoxCollection.sendKeys("CollectionNameChange");
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnSearch, 10);
		btnSearch.click();
		Log.message("Search button is clicked");
		Thread.sleep(5000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, collectionSelectionCheckBox, 15);
		if (collectionSelectionCheckBoxs.size() > 0) {
			ARCFNAAppUtils.waitForElement1(cloudDriver, collectionSelectionCheckBox, 10);
			collectionSelectionCheckBox.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnRemoveCollection, 10);
			btnRemoveCollection.click();

			for (String winHandle : cloudDriver.getWindowHandles()) {
				cloudDriver.switchTo().window(winHandle);
				String winurl = cloudDriver.switchTo().window(winHandle).getCurrentUrl();
			}

			ARCFNAAppUtils.waitForElement1(cloudDriver, btnYesDeleteCollection, 10);
			btnYesDeleteCollection.click();
			ARCFNAAppUtils.waitTill(5000);
		}

	}

	@SuppressWarnings("unused")
	public void changeCollectionName() {

		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitTill(5000);
		int rowSize = collectionlist.size();
		for (int i = 0; i <= rowSize; i++) {
			String collectionname = collectionlist.get(i).getText();
			Log.message(collectionname);
			if (collectionname.equalsIgnoreCase("CollectionName")) {
				int noOfDesiredCollection = i;
				// collectionlist.get(i).click();
				// ARCFNAAppUtils.waitTill(5000);
				editBtnList.get(i).click();
				ARCFNAAppUtils.waitTill(5000);

				for (String winHandle : cloudDriver.getWindowHandles()) {
					cloudDriver.switchTo().window(winHandle);
					String winurl = cloudDriver.switchTo().window(winHandle).getCurrentUrl();
				}

				ARCFNAAppUtils.waitForElement1(cloudDriver, txtProjectName, 20);
				txtProjectName.clear();
				txtProjectName.sendKeys("CollectionNameChanged");
				btnSaveCollection.click();
				ARCFNAAppUtils.waitTill(5000);
				Log.message("Collection has been added.");

				for (String winHandle : cloudDriver.getWindowHandles()) {
					cloudDriver.switchTo().window(winHandle);
					String winurl = cloudDriver.switchTo().window(winHandle).getCurrentUrl();
				}

				break;
			}
		}
	}

	public void markupDrawing() throws InterruptedException, AWTException {
		// Actions builder = new Actions(cloudDriver);
		// builder.moveToElement(imageViewer, 393,
		// 317).clickAndHold().moveToElement(imageViewer, 476, 383).build().perform();
		// builder.release().perform();
		Robot r1 = new Robot();
		r1.delay(2000);
		r1.mouseMove(393, 317);
		r1.delay(3000);
		r1.mousePress(InputEvent.BUTTON1_MASK);
		Log.message("123456");
		r1.delay(7000);
		r1.mouseMove(476, 383);
		r1.delay(7000);
		r1.mouseRelease(InputEvent.BUTTON1_MASK);
		r1.delay(7000);

	}

	public void createCollection() {
		
		String mainwindow = cloudDriver.getWindowHandle();
		ARCFNAAppUtils.waitTill(7000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, btnCollections, 10);
		btnCollections.click();
		ARCFNAAppUtils.waitTill(5000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitTill(5000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnAddCollections, 20);
		btnAddCollections.click();
		ARCFNAAppUtils.waitTill(5000);

		for (String childwindow1 : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(childwindow1);
			// String winurl = cloudDriver.switchTo().window(childwindow1).getCurrentUrl();
		}

		ARCFNAAppUtils.waitTill(5000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, txtProjectName, 20);
		txtProjectName.sendKeys("TestAutomationProject");
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnCollectionSave, 20);
		btnCollectionSave.click();
		//Log.message("Collection created");
		ARCFNAAppUtils.waitTill(8000);

		cloudDriver.switchTo().window(mainwindow);
		Log.message("switch to main window");
		//Log.message("001");

		ARCFNAAppUtils.waitTill(4000);
		Log.message("New Collection created successfully through CLOUD");
		
	}

	public void collectionNameChange() {
		ARCFNAAppUtils.waitTill(2000);

		String mainwindow = cloudDriver.getWindowHandle();
		ARCFNAAppUtils.waitTill(4000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, btnCollections, 20);
		btnCollections.click();
		ARCFNAAppUtils.waitTill(4000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitTill(4000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, fileSearchTextBox, 20);
		fileSearchTextBox.sendKeys("TestAutomationProject");

		ARCFNAAppUtils.waitForElement1(cloudDriver, btnSearchIcon, 20);
		btnSearchIcon.click();
		ARCFNAAppUtils.waitTill(6000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, btnCollectionEdit, 20);
		btnCollectionEdit.click();
		ARCFNAAppUtils.waitTill(5000);

		for (String childwindow1 : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(childwindow1);
		}
		ARCFNAAppUtils.waitTill(4000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, txtProjectName, 20);
		txtProjectName.clear();
		txtProjectName.sendKeys("CollectionNameChanged");

		ARCFNAAppUtils.waitForElement1(cloudDriver, btnCollectionSave, 20);
		btnCollectionSave.click();
		ARCFNAAppUtils.waitTill(5000);

		cloudDriver.switchTo().window(mainwindow);

		ARCFNAAppUtils.waitTill(5000);
	}

	/**
	 * Deleting collection
	 * 
	 * @throws InterruptedException
	 */

	public void deleteCollection() throws InterruptedException {
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnCollections, 10);
		btnCollections.click();
		ARCFNAAppUtils.waitTill(5000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitForElement1(cloudDriver, collectionSearchBox, 20);
		collectionSearchBox.sendKeys("TestAutomationProject");
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnSearchIcon, 20);
		btnSearchIcon.click();
		ARCFNAAppUtils.waitTill(5000);

		int cntCollection = collectionCheckboxes.size();

		if (cntCollection > 0) {
			ARCFNAAppUtils.waitForElement1(cloudDriver, collectionCheckbox, 20);
			collectionCheckbox.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, removeProject, 20);
			removeProject.click();
			ARCFNAAppUtils.waitTill(3000);
			for (String winHandle : cloudDriver.getWindowHandles()) {
				cloudDriver.switchTo().window(winHandle);
				String winurl = cloudDriver.switchTo().window(winHandle).getCurrentUrl();
			}
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnYesDeleteCollection, 20);
			btnYesDeleteCollection.click();
			ARCFNAAppUtils.waitTill(8000);
			cloudDriver.switchTo().defaultContent();
			Log.message("Existing Collection Deleted");
		} else {
			cloudDriver.switchTo().defaultContent();
			ARCFNAAppUtils.waitTill(2000);
			Log.message("Existing Collection Deleted");
		}
	}
	
	public void deleteNameChangedCollection() throws InterruptedException {
		ARCFNAAppUtils.waitTill(4000);
		String mainwindow = cloudDriver.getWindowHandle();
		ARCFNAAppUtils.waitTill(4000);
		
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnCollections, 10);
		btnCollections.click();
		ARCFNAAppUtils.waitTill(5000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitForElement1(cloudDriver, collectionSearchBox, 20);
		collectionSearchBox.sendKeys("CollectionNameChanged");
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnSearchIcon, 20);
		btnSearchIcon.click();
		ARCFNAAppUtils.waitTill(5000);

		int cntCollection = collectionCheckboxes.size();

		if (cntCollection > 0) {
			ARCFNAAppUtils.waitForElement1(cloudDriver, collectionCheckbox, 20);
			collectionCheckbox.click();
			ARCFNAAppUtils.waitForElement1(cloudDriver, removeProject, 20);
			removeProject.click();
			ARCFNAAppUtils.waitTill(3000);
			for (String childWindow1 : cloudDriver.getWindowHandles()) {
				cloudDriver.switchTo().window(childWindow1);
			}
			ARCFNAAppUtils.waitForElement1(cloudDriver, btnYesDeleteCollection, 20);
			btnYesDeleteCollection.click();
			ARCFNAAppUtils.waitTill(8000);
			cloudDriver.switchTo().window(mainwindow);
			ARCFNAAppUtils.waitTill(3000);
			Log.message("Collection Deleted");
			
			} 
		else {
			cloudDriver.switchTo().window(mainwindow);
			ARCFNAAppUtils.waitTill(3000);
			Log.message("Collection Deleted1");
		}
	}

	public void createFolder() {
		String mainwindow = cloudDriver.getWindowHandle();
		ARCFNAAppUtils.waitTill(7000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitTill(6000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, createFolder, 20);
		createFolder.click();
		ARCFNAAppUtils.waitTill(4000);

		for (String childwindow1 : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(childwindow1);
			// String winurl = cloudDriver.switchTo().window(childwindow1).getCurrentUrl();
		}

		ARCFNAAppUtils.waitTill(4000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitTill(4000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, folderNameTextbox, 20);
		folderNameTextbox.sendKeys("TestAutomationFolder");
		ARCFNAAppUtils.waitForElement1(cloudDriver, folderSave, 20);
		folderSave.click();
		ARCFNAAppUtils.waitTill(5000);

		cloudDriver.switchTo().window(mainwindow);
		ARCFNAAppUtils.waitTill(8000);
		Log.message("Folder created successfully through Cloud / Web");
	}

	public void createSubFolder() {
		String mainwindow = cloudDriver.getWindowHandle();
		ARCFNAAppUtils.waitTill(7000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitTill(6000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, createFolder, 20);
		createFolder.click();
		ARCFNAAppUtils.waitTill(4000);

		for (String childwindow1 : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(childwindow1);
			String winurl = cloudDriver.switchTo().window(childwindow1).getCurrentUrl();
		}
		ARCFNAAppUtils.waitTill(4000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitTill(4000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, folderNameTextbox, 20);
		folderNameTextbox.sendKeys("TestAutomationSubFolder");
		ARCFNAAppUtils.waitForElement1(cloudDriver, folderSave, 20);
		folderSave.click();
		ARCFNAAppUtils.waitTill(4000);

		cloudDriver.switchTo().window(mainwindow);
		Log.message("switch to main window");
		Log.message("001");

		ARCFNAAppUtils.waitTill(4000);

	}

	public void create2ndFolderUnderproject() {
		ARCFNAAppUtils.waitTill(4000);
		String mainwindow = cloudDriver.getWindowHandle();
		ARCFNAAppUtils.waitTill(7000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitForElement1(cloudDriver, testAutomationProject, 10);
		testAutomationProject.click();
		ARCFNAAppUtils.waitTill(2000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, createFolder, 20);
		createFolder.click();
		ARCFNAAppUtils.waitTill(4000);

		for (String childWindow1 : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(childWindow1);
		}
		ARCFNAAppUtils.waitTill(4000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitTill(4000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, folderNameTextbox, 20);
		folderNameTextbox.sendKeys("TestAutomationFolder1");
		ARCFNAAppUtils.waitForElement1(cloudDriver, folderSave, 20);
		folderSave.click();
		ARCFNAAppUtils.waitTill(5000);
		cloudDriver.switchTo().window(mainwindow);
		ARCFNAAppUtils.waitTill(5000);
	}

	public void createFolder1() {
		ARCFNAAppUtils.waitTill(6000);
		cloudDriver.switchTo().defaultContent();
		Log.message("Switch to dwfault content");
		String curWindow = cloudDriver.getWindowHandle();
		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitTill(6000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, createFolder, 20);
		createFolder.click();
		ARCFNAAppUtils.waitTill(4000);
		for (String winHandle : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(winHandle);
			String winurl = cloudDriver.switchTo().window(winHandle).getCurrentUrl();
		}
		ARCFNAAppUtils.waitTill(4000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitTill(4000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, folderNameTextbox, 20);
		folderNameTextbox.sendKeys("TestAutomationFolder1");
		ARCFNAAppUtils.waitForElement1(cloudDriver, folderSave, 20);
		folderSave.click();
		ARCFNAAppUtils.waitTill(5000);
		Log.message("Well Done");
		// cloudDriver.switchTo().window(curWindow);
		// Log.message(curWindow);
		// ARCFNAAppUtils.waitTill(5000);
	}

	public void fileMove() {
		ARCFNAAppUtils.waitTill(4000);
		String mainwindow = cloudDriver.getWindowHandle();
		ARCFNAAppUtils.waitTill(7000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitTill(3000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, folderColumnTree, 20);
		folderColumnTree.click();
		ARCFNAAppUtils.waitTill(3000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, firstFileCheckBox, 20);
		firstFileCheckBox.click();

		ARCFNAAppUtils.waitForElement1(cloudDriver, fileMoreButton, 20);
		fileMoreButton.click();

		ARCFNAAppUtils.waitForElement1(cloudDriver, moveFile, 20);
		moveFile.click();

		for (String childWindow1 : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(childWindow1);
		}

		ARCFNAAppUtils.waitTill(3000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitTill(4000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, targetFolder, 20);
		targetFolder.click();
		ARCFNAAppUtils.waitTill(4000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, moveFolderok, 20);
		moveFolderok.click();
		ARCFNAAppUtils.waitTill(4000);

		cloudDriver.switchTo().window(mainwindow);
		ARCFNAAppUtils.waitTill(4000);

	}

	public void fileCopy() {
		ARCFNAAppUtils.waitTill(4000);
		String mainwindow = cloudDriver.getWindowHandle();
		ARCFNAAppUtils.waitTill(7000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitTill(3000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, folderColumnTree, 20);
		folderColumnTree.click();
		ARCFNAAppUtils.waitTill(3000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, firstFileCheckBox, 20);
		firstFileCheckBox.click();

		ARCFNAAppUtils.waitForElement1(cloudDriver, fileMoreButton, 20);
		fileMoreButton.click();

		ARCFNAAppUtils.waitForElement1(cloudDriver, copyFile, 20);
		copyFile.click();

		for (String childWindow1 : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(childWindow1);
		}

		ARCFNAAppUtils.waitTill(3000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitTill(4000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, targetFolder, 20);
		targetFolder.click();
		ARCFNAAppUtils.waitTill(4000);

		ARCFNAAppUtils.waitForElement1(cloudDriver, moveFolderok, 20);
		moveFolderok.click();
		ARCFNAAppUtils.waitTill(4000);

		cloudDriver.switchTo().window(mainwindow);
		ARCFNAAppUtils.waitTill(4000);

	}

	public void fileSelection() {
		ARCFNAAppUtils.waitForElement1(cloudDriver, fileSelection, 20);
		fileSelection.click();
	}

	public void moveFile() {
		ARCFNAAppUtils.waitForElement1(cloudDriver, moreBtnFile, 20);
		moreBtnFile.click();
		ARCFNAAppUtils.waitForElement1(cloudDriver, moveFileBtn, 20);
		moveFileBtn.click();
		for (String winHandle : cloudDriver.getWindowHandles()) {
			cloudDriver.switchTo().window(winHandle);
			String winurl = cloudDriver.switchTo().window(winHandle).getCurrentUrl();
		}
		ARCFNAAppUtils.waitForElement1(cloudDriver, moveToFolder, 20);
		moveToFolder.click();
		ARCFNAAppUtils.waitForElement1(cloudDriver, moveFileOKbtn, 20);
		moveFileOKbtn.click();
		ARCFNAAppUtils.waitTill(8000);
	}
	
	public void openProjectAutomationTestingUseOnly()
	{
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnCollections, 10);
		btnCollections.click();
		ARCFNAAppUtils.waitTill(5000);
		ARCFNAAppUtils.waitForElement1(cloudDriver, frameMyFrame, 20);
		cloudDriver.switchTo().frame(frameMyFrame);
		ARCFNAAppUtils.waitForElement1(cloudDriver, collectionSearchBox, 20);
		collectionSearchBox.sendKeys("Automation_Testing_Use_only");
		ARCFNAAppUtils.waitForElement1(cloudDriver, btnSearchIcon, 20);
		btnSearchIcon.click();
		ARCFNAAppUtils.waitTill(7000);
		
		ARCFNAAppUtils.waitForElement1(cloudDriver, collectionSearched, 20);
		collectionSearched.click();
		ARCFNAAppUtils.waitTill(6000);
		Log.message("Desired Collection / Project opened successfully");
	}

}
