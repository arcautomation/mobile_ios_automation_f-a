package com.arc.fna.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.w3c.dom.Document;

import com.arc.fna.utils.ARCFNAAppUtils;
import com.arcautoframe.utils.Log;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class AlbumPage  extends LoadableComponent<AlbumPage> {
	
	public static AppiumDriver<MobileElement> driver;
	private boolean isPageLoaded;
	
	@iOSFindBy(accessibility="Delete Draft")
	WebElement btnDeleteMailDraft;
	
	@iOSFindBy(accessibility="Cancel")
	WebElement btnCancelOutlook;
	
	@iOSFindBy(accessibility="CreadeCumbCellLabel")
	WebElement btnNavigationProject;
	
	@iOSFindBy(accessibility="AlbumListSyncSwitch")
	WebElement btnAlbumPhotoSync;
	
	@iOSFindBy(accessibility="AlbumListImageTitle")
	WebElement btnPhoto;
	
	@iOSFindBy(accessibility="Use Photo")
	WebElement btnUsePhoto;
	
	@iOSFindBy(accessibility="PhotoCapture")
	WebElement btnPhotoCapture;
	
	@iOSFindBy(accessibility="moreIcon")
	WebElement btnThreeDottedMenu;
	
	
	@iOSFindBy(accessibility="moreIcon")
	WebElement btnThreeDottedMenuAlbum;
	
	@iOSFindBy(accessibility="Select")
	WebElement btnSelect;
	
	@iOSFindBy(accessibility="Create album")
	WebElement btnCreateAlbum;
	
	@iOSFindBy(accessibility="Share")
	WebElement btnShare;
	
	@iOSFindBy(accessibility="Delete")
	WebElement btnDelete;
	
	@iOSFindBy(accessibility="TestAlbum")
	WebElement btnSampleAlbum;
	
	@iOSFindBy(accessibility="AlbumCollectionView")
	WebElement panelAllAlbums;
	
	@iOSFindBy(accessibility="albumNameLbl")
	WebElement btnAlbum;
	
	@iOSFindBy(accessibility="Upload photos")
	WebElement btnUploadPhotos;
	

	@Override
	protected void load() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		
	}
	
	public AlbumPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		//PageFactory.initElements(this.driver, this);
	}
	
	public void clickThreeDottedButton()
	{
		ARCFNAAppUtils.waitForElement(driver, btnThreeDottedMenuAlbum, 20);
		//btnThreeDottedMenuAlbum.click();
		
		TouchAction action = new TouchAction(driver);
		action.press(991,84).release().perform();
		// action.tap(btnThreeDottedMenuAlbum).release().perform();
		action.release();
	}
	
	@SuppressWarnings("unused")
	public boolean deleteAlbum()
	{
		ARCFNAAppUtils.waitForElement(driver, panelAllAlbums, 20);
		int noOfAlbums = panelAllAlbums.findElements(By.name("albumNameLbl")).size();
		Log.message("No of albums Starts with"+ +noOfAlbums);
		if(noOfAlbums>0)
		{
		for(int i=0; i<=noOfAlbums; i++)
		{
			ARCFNAAppUtils.waitForElement(driver, btnThreeDottedMenuAlbum, 50);
			//TouchAction action6 = new TouchAction(driver);
			//action6.press(btnThreeDottedMenuAlbum).release().perform();
			btnThreeDottedMenuAlbum.click();
			
			ARCFNAAppUtils.waitForElement(driver, btnSelect, 25);
			btnSelect.click();
			
			ARCFNAAppUtils.waitForElement(driver, btnAlbum, 25);
			btnAlbum.click();
			
			ARCFNAAppUtils.waitForElement(driver, btnThreeDottedMenuAlbum, 50);
			//action6.press(btnThreeDottedMenuAlbum).release().perform();
			btnThreeDottedMenuAlbum.click();
			
			ARCFNAAppUtils.waitForElement(driver, btnDelete, 15);
			btnDelete.click();
			
			driver.switchTo().alert().accept();
			ARCFNAAppUtils.waitTill(2000);
			
			ARCFNAAppUtils.waitForElement(driver, panelAllAlbums, 20);
			Log.message("No of albums after deletion"+ +noOfAlbums);
			int noOfAlbums1 = panelAllAlbums.findElements(By.name("albumNameLbl")).size();
			ARCFNAAppUtils.waitTill(3000);
			if(noOfAlbums1==0)
			{	
				return true;
			}
			else
			{
				return false;
			}
		}
		}
		return true;
	}
	
	public boolean clickbtnCreateAlbum()
	{
		ARCFNAAppUtils.waitForElement(driver, btnThreeDottedMenuAlbum, 50);
		//TouchAction action6 = new TouchAction(driver);
		//action6.tap(btnThreeDottedMenuAlbum).perform();
		btnThreeDottedMenuAlbum.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnCreateAlbum, 20);
		btnCreateAlbum.click();
		
		driver.switchTo().alert();
		driver.getKeyboard().sendKeys("TestAlbum");
		driver.switchTo().alert().accept();
		ARCFNAAppUtils.waitForElement(driver, btnAlbum, 20);
		if(btnAlbum.isDisplayed())
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

	public void openAlbum()
	{
		ARCFNAAppUtils.waitForElement(driver, btnSampleAlbum, 15);
		btnSampleAlbum.click();
	}
	
	public boolean uploadPhoto()
	{
		ARCFNAAppUtils.waitForElement(driver, btnThreeDottedMenuAlbum, 50);
		btnThreeDottedMenuAlbum.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnUploadPhotos, 30);
		btnUploadPhotos.click();
		driver.switchTo().alert().accept();
		ARCFNAAppUtils.waitTill(2000);
		driver.switchTo().alert().accept();
		ARCFNAAppUtils.waitTill(3000);
		
		ARCFNAAppUtils.waitForElement(driver, btnPhotoCapture, 30);
		btnPhotoCapture.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnUsePhoto, 30);
		btnUsePhoto.click();
		ARCFNAAppUtils.waitTill(30000);
		
		ARCFNAAppUtils.waitForElement(driver, btnAlbumPhotoSync, 50);
		if(btnAlbumPhotoSync.isDisplayed() && btnAlbumPhotoSync.isEnabled())
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	public void returnAlbumPage()
	{
		ARCFNAAppUtils.waitForElement(driver, btnNavigationProject, 30);
		TouchAction action4 = new TouchAction(driver);
		//action4.tap(btnNavigationProject).release().perform();
		action4.press(161, 88).release().perform();
		action4.release();
		//btnNavigationProject.click();
	}
	
	public boolean sharingAlbum()
	{
		ARCFNAAppUtils.waitForElement(driver, btnThreeDottedMenuAlbum, 50);
		//TouchAction action6 = new TouchAction(driver);
		//action6.press(btnThreeDottedMenuAlbum).release().perform();
		btnThreeDottedMenuAlbum.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnSelect, 15);
		btnSelect.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnSampleAlbum, 15);
		btnSampleAlbum.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnThreeDottedMenuAlbum, 50);
		//action6.press(btnThreeDottedMenuAlbum).release().perform();
		btnThreeDottedMenuAlbum.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnShare, 20);
		btnShare.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnCancelOutlook, 40);
		if(btnCancelOutlook.isEnabled())
		{
			
			//action6.press(btnCancelOutlook).release().perform();
			btnCancelOutlook.click();
			
			ARCFNAAppUtils.waitForElement(driver, btnDeleteMailDraft, 40);
			//action6.press(btnDeleteMailDraft).release().perform();
			btnDeleteMailDraft.click();
			
			return true;
		}
		else
		{
			return false;
		}
		
		
	}
	
	public boolean sharingPhoto()
	{
		ARCFNAAppUtils.waitForElement(driver, btnThreeDottedMenuAlbum, 50);
		//TouchAction action6 = new TouchAction(driver);
		//action6.press(btnThreeDottedMenuAlbum).release().perform();
		btnThreeDottedMenuAlbum.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnSelect, 15);
		btnSelect.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnPhoto, 20);
		btnPhoto.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnThreeDottedMenuAlbum, 50);
		//action6.press(btnThreeDottedMenuAlbum).release().perform();
		btnThreeDottedMenuAlbum.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnShare, 20);
		btnShare.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnCancelOutlook, 20);
		if(btnCancelOutlook.isEnabled())
		{
			//action6.press(btnCancelOutlook).release().perform();
			btnCancelOutlook.click();
			
			ARCFNAAppUtils.waitForElement(driver, btnDeleteMailDraft, 40);
			//action6.press(btnDeleteMailDraft).release().perform();
			btnDeleteMailDraft.click();
			
			return true;
		}
		else
		{
			return false;
		}
		
		
	}
	
}

