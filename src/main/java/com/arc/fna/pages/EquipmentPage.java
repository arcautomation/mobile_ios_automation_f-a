package com.arc.fna.pages;

import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Rotatable;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DriverCommand;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.arc.fna.utils.ARCFNAAppUtils;
import com.arcautoframe.utils.Log;
import com.google.common.collect.ImmutableMap;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class EquipmentPage extends LoadableComponent<EquipmentPage> {

	/* Object Instantiation block */

	public static AppiumDriver<MobileElement> driver;
	private boolean isPageLoaded;
	EquipmentPage equipmentPage;
	public static String equipName;
	public static String equipDesc;
	public static String equipManu;
	public static String equipMod;
	public static String equipSl;
	public static String equipType;
	static String secondLastEquip;

	/*
	 * Object identification block
	 */

	@iOSFindBy(accessibility = "Equipment")
	WebElement equimentCaption;

	@iOSFindBy(accessibility = "EquipmentNavigationBarLeftBtn")
	WebElement btnMenu;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='EquipmentNavigationBarLeftBtn'])[3]")
	WebElement btnCloseQRWindow;

	@iOSFindBy(accessibility = "SignOutMenuBtn")
	WebElement btnSignOut;

	@iOSFindBy(accessibility = "EquipmentNavigationBarRightBtn")
	WebElement btnAddEquipment;

	@iOSFindBy(accessibility = "AssetDetailsIconPhotoVideoCellFullBtn")
	WebElement btnAddPhotoEquipment;

	@iOSFindBy(accessibility = "OK")
	//@iOSFindBy(accessibility = "Allow")
	List<WebElement> btnOKPhotoAccessList;

	@iOSFindBy(accessibility = "OK")
	//@iOSFindBy(accessibility = "Allow")
	WebElement btnOKPhotoAccess;
	
	@iOSFindBy(accessibility = "Allow")
	WebElement btnAllowPhotoAccess;
	
	@iOSFindBy(accessibility = "Allow")
	List<WebElement> btnsAllowPhotoAccess;

	@iOSFindBy(accessibility = "CustomCameraVCCameraMainBtn")
	WebElement btnCameraEquipment;

	@iOSFindBy(accessibility = "CustomCameraVCImageBtn")
	WebElement btnMarkupDraw;

	@iOSFindBy(accessibility = "PWPImageHeaderViewDoneBtn")
	WebElement btnMarkupDone;

	@iOSFindBy(accessibility = "PWPImageHeaderViewDeleteBtn")
	WebElement btnMarkupDelete;

	@iOSFindBy(accessibility = "PWPImageHeaderViewCloseBtn")
	WebElement btnMarkupClose;

	@iOSFindBy(accessibility = "AssetDetailsInstructionCellTitleLbl")
	List<WebElement> lblEquipmentInfoList;

	@iOSFindBy(accessibility = "AssetDetailsInstructionCellTitleLbl")
	WebElement lblEquipmentInfo;

	@iOSFindBy(accessibility = "AssetDetailsInstructionCellInstructionBtn")
	List<WebElement> btnsEquipmentAllDetailsAdd;

	@iOSFindBy(accessibility = "AssetDetailsInstructionCellInstructionBtn")
	WebElement btnEquipmentAllDetailsAdd;

	@iOSFindBy(accessibility = "ImageViewerHeaderCellSelectBtn_1006")
	WebElement btnLineDraw;

	@iOSFindBy(accessibility = "Name")
	WebElement txtEquipmentName;

	@iOSFindBy(accessibility = "Description")
	WebElement txtEquipmentDesc;

	@iOSFindBy(accessibility = "Install date")
	WebElement txtEquipmentInstDate;

	@iOSFindBy(accessibility = "Warranty date")
	WebElement txtEquipmentWrntDate;

	@iOSFindBy(accessibility = "Manufacturer")
	WebElement txtEquipmentManufacturer;

	@iOSFindBy(accessibility = "Model")
	WebElement txtEquipmentModel;

	@iOSFindBy(accessibility = "Serial number")
	WebElement txtEquipmentSerialNo;

	@iOSFindBy(accessibility = "Type")
	WebElement txtEquipmentType;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='EquipmentNavigationBarRightBtn'])[2]")
	WebElement btnEquiDetailsDone;

	@iOSFindBy(accessibility = "Save")
	WebElement btnEquiSave;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='EquipmentNavigationBarLeftBtn'])[2]")
	// @iOSFindBy(accessibility="back")
	WebElement btnEquiSaveBack;

	@iOSFindBy(accessibility = "EquipmentNavigationBarLeftBtn")
	WebElement btnEquiSearchBack;

	// @iOSFindBy(accessibility="Search")
	// @iOSFindBy(accessibility="EquipmentSearchViewSearchBar")
	@iOSFindBy(xpath = "//XCUIElementTypeSearchField[@name='Search']")
	WebElement txtGlobalSearchEquip;

	@iOSFindBy(accessibility = "EquipmentSearchViewSearchBar")
	WebElement txtGlobalSearchEquipAfterFilter;

	@iOSFindBy(xpath = "//XCUIElementTypeSearchField[@name='Search']")
	WebElement txtGlobalSearchEquipAfterRecentlyViewed;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='AssetDetailsAssetCellAssetNameLbl'])[1]")
	List<WebElement> lblNameSearchList;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='AssetDetailsAssetCellAssetNameLbl'])[1]")
	WebElement lblNameSearch;

	@iOSFindBy(accessibility = "Done")
	WebElement btnDateDone;

	@iOSFindBy(accessibility = "AssetDetailsDisclosureCellTitleLbl")
	public static WebElement btnEquipDetailsEditMode;

	@iOSFindBy(accessibility = "Location")
	public static WebElement btnLocationEditMode;

	@iOSFindBy(accessibility = "Name")
	public static WebElement btnEquipNameEditMode;

	@iOSFindBy(accessibility = "Name")
	public static List<WebElement> btnEquipNameEditModeList;

	@iOSFindBy(accessibility = "Description")
	public static WebElement btnEquipDescEditMode;

	@iOSFindBy(accessibility = "Description")
	public static List<WebElement> btnEquipDescEditModeList;

	@iOSFindBy(accessibility = "Manufacturer")
	public static WebElement btnEquipManuEditMode;

	@iOSFindBy(accessibility = "Model")
	public static WebElement btnEquipModelEditMode;

	@iOSFindBy(accessibility = "Serial number")
	public static WebElement btnEquipSlNoEditMode;

	@iOSFindBy(accessibility = "Type")
	public static WebElement btnEquipTypeEditMode;

	@iOSFindBy(accessibility = "Building")
	public static WebElement lblBuildingEditMode;

	@iOSFindBy(accessibility = "Floor")
	public static WebElement lblFloorEditMode;

	@iOSFindBy(accessibility = "Area")
	public static WebElement lblAreaEditMode;

	// @iOSFindBy(accessibility="EquipmentTableHeaderFilterArrowImageView")
	// @iOSFindBy(accessibility="Filter")
	@iOSFindBy(accessibility = "EquipmentTableHeaderFilterLbl")
	WebElement btnFilterSearchDropDown;

	// @iOSFindBy(accessibility="Equipment name")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Equipment name']")
	public static WebElement btnRadioFilterSearchEquipName;

	// @iOSFindBy(accessibility="Serial number")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Serial number']")
	public static WebElement btnRadioFilterSearchSlNo;

	// @iOSFindBy(accessibility="Description")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Description']")
	public static WebElement btnRadioFilterSearchDesc;

	// @iOSFindBy(accessibility="Building")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Building']")
	public static WebElement btnRadioFilterSearchBld;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Floor']")
	// @iOSFindBy(accessibility="Floor")
	public static WebElement btnRadioFilterSearchFlr;

	// @iOSFindBy(accessibility="Ärea")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Area']")
	public static WebElement btnRadioFilterSearchArea;

	// @iOSFindBy(accessibility="Model")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Model']")
	public static WebElement btnRadioFilterSearchModel;

	// @iOSFindBy(accessibility="Manufacturer")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Manufacturer']")
	public static WebElement btnRadioFilterSearchManufacturer;

	// @iOSFindBy(accessibility="Documents")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Documents']")
	public static WebElement btnRadioFilterSearchDocuments;

	// @iOSFindBy(accessibility="Reports")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Reports']")
	public static WebElement btnRadioFilterSearchReports;

	// @iOSFindBy(accessibility="Parts")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Parts']")
	public static WebElement btnRadioFilterSearchParts;

	@iOSFindBy(accessibility = "AssetDetailsEquipmentCellAssetNameLbl")
	List<WebElement> lblEquipNameListAfterAdd;

	//@iOSFindBy(accessibility = "AssetDetailsEquipmentCellAssetNameLbl")
	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='AssetDetailsEquipmentCellAssetNameLbl'])[1]")
	WebElement lblEquipNameList;
	
	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='AssetDetailsEquipmentCellAssetNameLbl'])[3]")
	WebElement lblEquipNameList2;

	@iOSFindBy(accessibility = "AssetTagControllerAddTagTextField")
	WebElement txtTagDetails;

	@iOSFindBy(accessibility = "Done")
	WebElement doneTagSelection;

	@iOSFindBy(accessibility = "Edit")
	WebElement btnEquipEdit;

	@iOSFindBy(accessibility = "EquipmentNavigationBarMoreBtn")
	WebElement btnEquipMore;

	@iOSFindBy(accessibility = "Delete")
	WebElement btnEquipDelete;

	@iOSFindBy(accessibility = "AssetBaseElementDetailsBottomBtn")
	WebElement btnGenerateQRCode;

	@iOSFindBy(accessibility = "Save")
	public static WebElement btnSaveQRCode;

	@iOSFindBy(accessibility = "Email")
	public static WebElement btnEmailQRCode;

	@iOSFindBy(accessibility = "Print")
	public static WebElement btnPrintQRCode;

	@iOSFindBy(accessibility = "toField")
	WebElement qrMailToFiled;

	@iOSFindBy(accessibility = "Send")
	WebElement btnQRMailSend;

	@iOSFindBy(accessibility = "Cancel")
	WebElement btnQRMailCancel;

	@iOSFindBy(accessibility = "Delete Draft")
	WebElement btnDeleteDraft;

	@iOSFindBy(accessibility = "Cancel")
	WebElement btnQRPrintCancel;

	@iOSFindBy(accessibility = "GALLERY")
	WebElement btnGallery;

	@iOSFindBy(xpath = "Camera Roll")
	WebElement btnCameraRoll;

	@iOSFindBy(accessibility = "CCAssetCellImageView")
	List<WebElement> btnPhotoFiles;

	@iOSFindBy(accessibility = "CCAssetCellImageView")
	WebElement btnPhotoFile;

	// @iOSFindBy(accessibility="Done")
	// WebElement btnEquiDetailsDone;

	@Override
	protected void load() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub

	}

	/*
	 * Constructor definition block
	 */

	public EquipmentPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	/*
	 * Behavior or functionality definition block
	 */

	public boolean verifyEquipmentModulelaunch() {
		Log.message("Checking whether equipment caption is visible?");
		ARCFNAAppUtils.waitForElement(driver, equimentCaption, 30);
		if (equimentCaption.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}

	public void equipmentAdd() {
		ARCFNAAppUtils.waitForElement(driver, btnAddEquipment, 40);
		btnAddEquipment.click();
		Log.message("Equipment Add button has been clicked");
	}

	public void equipmentPhotoAddWithLineMarkup() {
		ARCFNAAppUtils.waitForElement(driver, btnAddPhotoEquipment, 20);
		btnAddPhotoEquipment.click();
		ARCFNAAppUtils.waitTill(6000);
		
		WebDriverWait wait = new WebDriverWait(driver, 3);

		ARCFNAAppUtils.waitForElement(driver, btnOKPhotoAccess, 40);
		int okCount = btnOKPhotoAccessList.size();
		
		
		if (okCount > 0) {
			btnOKPhotoAccess.click();
			ARCFNAAppUtils.waitTill(1000);
			btnOKPhotoAccess.click();
			ARCFNAAppUtils.waitTill(1000);
			btnOKPhotoAccess.click();
			ARCFNAAppUtils.waitTill(1000);
			}
		
		ARCFNAAppUtils.waitForElement(driver, btnAllowPhotoAccess, 20);
		int allowCnt = btnsAllowPhotoAccess.size();
		if(allowCnt>0)
		{
			btnAllowPhotoAccess.click();
			ARCFNAAppUtils.waitTill(2000);
		}
		
			/*do {
				driver.switchTo().alert().accept();
				ARCFNAAppUtils.waitTill(8000);
				Log.message("Button Cliked");
				Log.message(ExpectedConditions.alertIsPresent().toString());
			}
			while(okCount>0);*/
			
			/*do {
				driver.switchTo().alert().accept();
				ARCFNAAppUtils.waitTill(8000);
				Log.message("Button Cliked");
				Log.message(ExpectedConditions.alertIsPresent().toString());
			}
			while(allowCnt>0);*/
		
			
		ARCFNAAppUtils.waitTill(2000);
		ARCFNAAppUtils.waitForElement(driver, btnCameraEquipment, 40);
		Log.message("Done");
		btnCameraEquipment.click();
		Log.message("Done1");
		Log.message("Photo has been taken for the desired Equipment");
		ARCFNAAppUtils.waitForElement(driver, btnMarkupDraw, 40);
		btnMarkupDraw.click();
		ARCFNAAppUtils.waitForElement(driver, btnLineDraw, 40);
		btnLineDraw.click();
		TouchAction action1 = new TouchAction(driver).longPress(400, 184).moveTo(607, 571).release().perform();
		Log.message("Mark up has been taken for the desired Equipment");
		ARCFNAAppUtils.waitForElement(driver, btnMarkupDone, 40);
		btnMarkupDone.click();
		Log.message("Mark up has been drawn successfully");
		ARCFNAAppUtils.waitTill(6000);
		// driver.rotate(ScreenOrientation.LANDSCAPE);

		ScreenOrientation orientation = driver.getOrientation();
		String orentValue = orientation.value();
		Log.message(orentValue);

	}

	public void selectEquipmentComponent(String infoLabel) {
		String equipID = infoLabel;
		WebElement equipComp = driver.findElement(By.id(equipID));
		switch (infoLabel) {
		case "Equipment details":
			equipComp.click();
			break;

		case "Location":
			equipComp.click();
			break;

		case "Maintenance/Inspection reports":
			equipComp.click();
			break;

		case "Linked files":
			equipComp.click();
			break;

		case "Parts":
			equipComp.click();
			break;

		case "Useful links":
			equipComp.click();
			break;

		case "Notes":
			equipComp.click();
			break;

		case "Tags":
			equipComp.click();
			break;
		}

	}

	public void selectEquipmentTag(String infoTag) {
		ARCFNAAppUtils.waitTill(25000);
		String tag = infoTag;
		WebElement tagName = driver.findElement(By.id(tag));
		ARCFNAAppUtils.waitTill(5000);
		switch (infoTag) {
		case "ac":
			tagName.click();
			Log.message("Tag selection 001");
			break;

		case "samsung":
			tagName.click();
			break;
		}
	}

	public void doneTagSelection() {
		ARCFNAAppUtils.waitForElement(driver, doneTagSelection, 20);
		doneTagSelection.click();
	}

	public void equipmentDetailsAdd() {

		// Random rand = new Random();
		ARCFNAAppUtils.waitForElement(driver, txtEquipmentName, 40);
		txtEquipmentName.clear();
		txtEquipmentName.sendKeys("Equip" + Math.round(Math.random() * 10000));

		equipName = txtEquipmentName.getText();

		ARCFNAAppUtils.waitForElement(driver, txtEquipmentDesc, 40);
		txtEquipmentDesc.clear();
		txtEquipmentDesc.sendKeys("Desc" + Math.round(Math.random() * 10000));

		equipDesc = txtEquipmentDesc.getText();

		ARCFNAAppUtils.waitForElement(driver, txtEquipmentInstDate, 40);
		txtEquipmentInstDate.click();

		ARCFNAAppUtils.waitForElement(driver, btnDateDone, 40);
		btnDateDone.click();

		ARCFNAAppUtils.waitForElement(driver, txtEquipmentWrntDate, 40);
		txtEquipmentWrntDate.click();

		ARCFNAAppUtils.waitForElement(driver, btnDateDone, 40);
		btnDateDone.click();

		ARCFNAAppUtils.waitForElement(driver, txtEquipmentManufacturer, 40);
		txtEquipmentManufacturer.clear();
		;
		txtEquipmentManufacturer.sendKeys("Manu" + Math.round(Math.random() * 10000));

		equipManu = txtEquipmentManufacturer.getText();

		driver.hideKeyboard();

		ARCFNAAppUtils.waitForElement(driver, txtEquipmentModel, 40);
		txtEquipmentModel.clear();
		;
		txtEquipmentModel.sendKeys("Mod" + Math.round(Math.random() * 10000));

		equipMod = txtEquipmentModel.getText();

		driver.hideKeyboard();

		ARCFNAAppUtils.waitForElement(driver, txtEquipmentSerialNo, 40);
		txtEquipmentSerialNo.clear();
		;
		txtEquipmentSerialNo.sendKeys("SL" + Math.round(Math.random() * 10000));

		equipSl = txtEquipmentSerialNo.getText();

		driver.hideKeyboard();

		ARCFNAAppUtils.waitForElement(driver, txtEquipmentType, 40);
		txtEquipmentType.clear();
		;
		txtEquipmentType.sendKeys("Type" + Math.round(Math.random() * 10000));

		equipType = txtEquipmentType.getText();

		ARCFNAAppUtils.waitForElement(driver, btnEquiDetailsDone, 40);
		btnEquiDetailsDone.click();

		Log.message("Equipment details entered successfully");

	}

	public void equipmentTagAdd(String tagName1, String tagName2) {
		ARCFNAAppUtils.waitForElement(driver, txtTagDetails, 30);
		txtTagDetails.sendKeys(tagName1, tagName2);
		ARCFNAAppUtils.waitForElement(driver, btnEquiSaveBack, 30);
		btnEquiSaveBack.click();
		Log.message("Tag details saved successfully");
	}

	public void equiSave() {
		ARCFNAAppUtils.waitForElement(driver, btnEquiSave, 40);
		btnEquiSave.click();
	}

	public void equipSaveClose() {
		ARCFNAAppUtils.waitForElement(driver, btnEquiSaveBack, 40);
		btnEquiSaveBack.click();
		Log.message("Equipment details saved successfully");

	}

	public void searchEquipment(String searchString) {
		ARCFNAAppUtils.waitForElement(driver, txtGlobalSearchEquip, 40);

		int leftX = txtGlobalSearchEquip.getLocation().getX();
		int rightX = leftX + txtGlobalSearchEquip.getSize().getWidth();
		int middleX = (rightX + leftX) / 2;
		int upperY = txtGlobalSearchEquip.getLocation().getY();
		int lowerY = upperY + txtGlobalSearchEquip.getSize().getHeight();
		int middleY = (upperY + lowerY) / 2;
		Log.message(Integer.toString(middleX));
		Log.message(Integer.toString(middleY));
		ARCFNAAppUtils.waitTill(3000);

		TouchAction t1 = new TouchAction(driver);
		// t1.press(198, 150).release().perform();
		t1.press(middleX, middleY).release().perform();
		ARCFNAAppUtils.waitTill(3000);
		txtGlobalSearchEquip.sendKeys(searchString);
		driver.getKeyboard().sendKeys(Keys.chord(Keys.ENTER));
		ARCFNAAppUtils.waitTill(8000);
	}

	public void searchEquipmentSearchView(String searchString) {
		ARCFNAAppUtils.waitForElement(driver, txtGlobalSearchEquipAfterFilter, 40);

		int leftX = txtGlobalSearchEquipAfterFilter.getLocation().getX();
		int rightX = leftX + txtGlobalSearchEquipAfterFilter.getSize().getWidth();
		int middleX = (rightX + leftX) / 2;
		int upperY = txtGlobalSearchEquipAfterFilter.getLocation().getY();
		int lowerY = upperY + txtGlobalSearchEquipAfterFilter.getSize().getHeight();
		int middleY = (upperY + lowerY) / 2;
		Log.message(Integer.toString(middleX));
		Log.message(Integer.toString(middleY));
		ARCFNAAppUtils.waitTill(3000);

		TouchAction t1 = new TouchAction(driver);
		// t1.press(198, 150).release().perform();
		t1.press(middleX, middleY).release().perform();
		ARCFNAAppUtils.waitTill(3000);
		txtGlobalSearchEquipAfterFilter.sendKeys(searchString);
		driver.getKeyboard().sendKeys(Keys.chord(Keys.ENTER));
		ARCFNAAppUtils.waitTill(8000);
	}

	public void searchAfterFilterApplied(String searchString) {
		ARCFNAAppUtils.waitForElement(driver, txtGlobalSearchEquipAfterFilter, 40);
		// txtGlobalSearchEquip.click();
		// txtGlobalSearchEquip.sendKeys(equipName);
		// driver.getKeyboard();

		int leftX = txtGlobalSearchEquipAfterFilter.getLocation().getX();
		int rightX = leftX + txtGlobalSearchEquipAfterFilter.getSize().getWidth();
		int middleX = (rightX + leftX) / 2;
		int upperY = txtGlobalSearchEquipAfterFilter.getLocation().getY();
		int lowerY = upperY + txtGlobalSearchEquipAfterFilter.getSize().getHeight();
		int middleY = (upperY + lowerY) / 2;
		Log.message(Integer.toString(middleX));
		Log.message(Integer.toString(middleY));
		ARCFNAAppUtils.waitTill(3000);

		TouchAction t1 = new TouchAction(driver);
		t1.press(middleX, middleY).release().perform();
		ARCFNAAppUtils.waitTill(3000);
		// txtGlobalSearchEquip.sendKeys(equipName);
		// txtGlobalSearchEquipAfterFilter.clear();
		// txtGlobalSearchEquipAfterFilter.sendKeys(searchString);
		driver.getKeyboard().sendKeys(Keys.chord(Keys.ENTER));
		ARCFNAAppUtils.waitTill(5000);
	}

	/*
	 * public boolean verifySearchandSaveEquipment() {
	 * ARCFNAAppUtils.waitForElement(driver, lblNameSearch, 40); int lblEquicount =
	 * lblNameSearchList.size(); Log.message(Integer.toString(lblEquicount));
	 * for(int i=0; i<=lblEquicount; i++) { String lblName =
	 * lblNameSearch.getText(); if(lblName.equals(equipName)) {
	 * btnEquiSearchBack.click();
	 * Log.message("Equipment found successfully while searching with: "+
	 * searchString); return true; } else { btnEquiSearchBack.click();
	 * Log.message("Equipment search failed while searching with: "+ searchString);
	 * return false; } }
	 * 
	 * }
	 */

	public boolean verifySearchWithEquipSearchParameter(/* List<WebElement> elementList, */ WebElement element,
			String searchString) {
		ARCFNAAppUtils.waitForElement(driver, lblNameSearch, 40);
		lblNameSearch.click();

		ARCFNAAppUtils.waitForElement(driver, btnEquipDetailsEditMode, 40);
		btnEquipDetailsEditMode.click();

		ARCFNAAppUtils.waitForElement(driver, element, 40);
		// int cntElementList = elementList.size();
		// for(int i = 0; i<=cntElementList; i++)
		// {

		if (searchString.equals(element.getText())) {
			Log.message("We found Search String value: " + searchString + " >> And desired field value: "
					+ element.getText());
			ARCFNAAppUtils.waitForElement(driver, btnEquiSaveBack, 40);
			btnEquiSaveBack.click();
			ARCFNAAppUtils.waitTill(1000);
			btnEquiSaveBack.click();
			ARCFNAAppUtils.waitForElement(driver, btnEquiSearchBack, 40);
			btnEquiSearchBack.click();
			return true;
		} else {
			Log.message("We found Search String value: " + searchString + " >> And desired field value: "
					+ element.getText());
			ARCFNAAppUtils.waitForElement(driver, btnEquiSaveBack, 40);
			btnEquiSaveBack.click();
			ARCFNAAppUtils.waitTill(1000);
			btnEquiSaveBack.click();
			ARCFNAAppUtils.waitForElement(driver, btnEquiSearchBack, 40);
			btnEquiSearchBack.click();
			return false;
		}

		// }
		// return true;
	}

	public boolean verifySearchWithEquipAndTagSearchParameter(String equipID, String searchString) {
		// List<MobileElement> equipNameIDs = driver.findElements(By.id(equipID));
		// int eleCount = equipNameIDs.size();

		// List<MobileElement> equipNameIDs = driver.findElements(By.id(equipID));
		// int eleCount = equipNameIDs.size();
		
		int i = 0;
		do {
			// Log.message("Size of initial count " + eleCount);
			driver.swipe(506, 635, 488, 187, 1000);
			ARCFNAAppUtils.waitTill(3000);
			// Log.message("After first swipe " + eleCount);
			i++;
		} while (driver.findElements(By.id(equipID)).size() != 1);

		ARCFNAAppUtils.waitTill(2000);
		Log.message(equipID + " " + searchString);
		WebElement equipNameID = driver.findElement(By.id(equipID));
		if (equipNameID.getText().equals(searchString)) {
			Log.message("1234567");
			ARCFNAAppUtils.waitForElement(driver, btnMenu, 20);
			btnMenu.click();
			ARCFNAAppUtils.waitTill(4000);
			return true;
		} else {
			ARCFNAAppUtils.waitForElement(driver, btnMenu, 20);
			btnMenu.click();
			ARCFNAAppUtils.waitTill(4000);
			return false;
		}

		// WebElement equipNameID = driver.findElement(By.id(equipID));
		// Log.message(equipID +" "+ searchString);

		// if(equipNameID.getText().equals(searchString))
		// {
		// ARCFNAAppUtils.waitForElement(driver, btnMenu, 20);
		// btnMenu.click();
		// ARCFNAAppUtils.waitTill(4000);
		// return true;
		// }
		// else
		// {
		// ARCFNAAppUtils.waitForElement(driver, btnMenu, 20);
		// btnMenu.click();
		// ARCFNAAppUtils.waitTill(4000);
		// return false;
		// }
	}

	public void selectFilterSearchCriteria(WebElement element2) {
		ARCFNAAppUtils.waitForElement(driver, btnFilterSearchDropDown, 30);
		btnFilterSearchDropDown.click();

		ARCFNAAppUtils.waitForElement(driver, element2, 30);
		element2.click();

		Log.message("***********&&&&&&&&%%%%%%%");

		ARCFNAAppUtils.waitForElement(driver, btnFilterSearchDropDown, 30);
		btnFilterSearchDropDown.click();

		ARCFNAAppUtils.waitTill(2000);

	}

	public boolean verifyFilterSearch(WebElement searchElement, String searchString) {
		ARCFNAAppUtils.waitForElement(driver, searchElement, 40);
		String elementText = searchElement.getText();

		Log.message("Field name: " + elementText);

		String filterMatch = elementText.substring(0, 5);

		if (searchString.equals(filterMatch)) {
			ARCFNAAppUtils.waitForElement(driver, btnEquiSaveBack, 40);
			btnEquiSaveBack.click();
			ARCFNAAppUtils.waitTill(1000);
			btnEquiSaveBack.click();
			ARCFNAAppUtils.waitForElement(driver, btnEquiSearchBack, 40);
			btnEquiSearchBack.click();
			return true;
		} else {
			ARCFNAAppUtils.waitForElement(driver, btnEquiSaveBack, 40);
			btnEquiSaveBack.click();
			ARCFNAAppUtils.waitTill(1000);
			btnEquiSaveBack.click();
			ARCFNAAppUtils.waitForElement(driver, btnEquiSearchBack, 40);
			btnEquiSearchBack.click();
			return false;
		}

	}

	public boolean verifyLastCreatedEquipAppearanceInList(String eqpname) {
		//int cntEquip = lblEquipNameListAfterAdd.size();
		//for (int i = 0; i <= cntEquip; i++) {
			ARCFNAAppUtils.waitForElement(driver, lblEquipNameList, 40);
			String equipName = lblEquipNameList.getText();
			ARCFNAAppUtils.waitTill(4000);
			if (equipName.equals(eqpname) /*&& i == 0*/) {
				Log.message("Displayed equipment name: " + equipName + " Last Entered equipment name: " + eqpname
						/*+ " Position row wise " + Integer.toString(i)*/);
				return true;
			} else {
				Log.message("Displayed equipment name: " + equipName + " Last Entered equipment name: " + eqpname
						/*+ " Position row wise " + Integer.toString(i)*/);
				return false;
			}

		}
		//return true;
	//}

	public boolean verifyLastViewedEquipAppearanceInList() {
		int equipCnt = lblEquipNameListAfterAdd.size();
		for (int i = 0; i <= equipCnt; i++) {
			if (i == 1) {
				ARCFNAAppUtils.waitForElement(driver, lblEquipNameList, 40);
				String secondEquip = lblEquipNameList.getText();
				lblEquipNameList.click();

				ARCFNAAppUtils.waitForElement(driver, btnEquiSaveBack, 40);
				btnEquiSaveBack.click();

				for (int j = 0; j <= equipCnt; j++) {
					if (j == 0 && lblEquipNameList.getText().equals(secondEquip)) {
						Log.message("Position: " + j + " Asset appeared: " + lblEquipNameList.getText());
						return true;
					} else {
						Log.message("Position: " + j + " Asset appeared: " + lblEquipNameList.getText());
						return false;
					}
				}

			}
		}
		return true;
	}
	
	public boolean verifyLastViewedEquipAppearanceInList1() {
		
		ARCFNAAppUtils.waitForElement(driver, lblEquipNameList2, 40);
		String secondEquip = lblEquipNameList2.getText();
		Log.message("****** Equipment clicked: " + secondEquip);
		lblEquipNameList2.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnEquiSaveBack, 40);
		btnEquiSaveBack.click();
		ARCFNAAppUtils.waitTill(3000);
		
		if(lblEquipNameList.getText().equals(secondEquip))
		{
			return true;
		}
		else
		{
			return false;
		}
	
	}
	

	public void openSearchItem() {
		ARCFNAAppUtils.waitForElement(driver, lblNameSearch, 30);
		lblNameSearch.click();
	}

	public void equipEditClick() {
		ARCFNAAppUtils.waitForElement(driver, btnEquipEdit, 30);
		btnEquipEdit.click();
	}

	public void equipDelete() {
		ARCFNAAppUtils.waitForElement(driver, btnEquipMore, 30);
		btnEquipMore.click();

		ARCFNAAppUtils.waitForElement(driver, btnEquipDelete, 30);
		btnEquipDelete.click();
	}

	public boolean verifyDeleteEquipCount() {
		int cntSearchedEquip = lblNameSearchList.size();
		if (cntSearchedEquip < 1) {
			ARCFNAAppUtils.waitForElement(driver, btnMenu, 30);
			btnMenu.click();
			return true;
		} else {
			ARCFNAAppUtils.waitForElement(driver, btnMenu, 30);
			btnMenu.click();
			return false;
		}
	}

	public void clickQRCodeGeneration() {
		ARCFNAAppUtils.waitForElement(driver, btnGenerateQRCode, 30);
		btnGenerateQRCode.click();
	}

	public void exportQRCode(WebElement element) {
		ARCFNAAppUtils.waitForElement(driver, btnEquiDetailsDone, 30);
		btnEquiDetailsDone.click();
		ARCFNAAppUtils.waitForElement(driver, element, 30);
		element.click();
	}

	public boolean verifyQRCodeSave() {
		String expQRSaveMsg = driver.switchTo().alert().getText();
		String actQRSaveMsg = "Your image has been saved to your photos.";

		Log.message("Actual: " + actQRSaveMsg);
		Log.message("Expected: " + expQRSaveMsg);

		if (expQRSaveMsg.equals(actQRSaveMsg)) {
			driver.switchTo().alert().accept();
			return true;
		} else {
			driver.switchTo().alert().accept();
			return false;
		}
	}

	public boolean verifyQRCodeEmail(String mailID) {
		ARCFNAAppUtils.waitForElement(driver, qrMailToFiled, 30);
		qrMailToFiled.sendKeys(mailID);
		ARCFNAAppUtils.waitTill(2000);

		ARCFNAAppUtils.waitForElement(driver, btnQRMailSend, 30);
		if (btnQRMailSend.isDisplayed() == true) {
			ARCFNAAppUtils.waitForElement(driver, btnQRMailCancel, 30);
			btnQRMailCancel.click();
			ARCFNAAppUtils.waitForElement(driver, btnDeleteDraft, 30);
			btnDeleteDraft.click();
			return true;
		} else {
			ARCFNAAppUtils.waitForElement(driver, btnQRMailCancel, 30);
			btnQRMailCancel.click();
			ARCFNAAppUtils.waitForElement(driver, btnDeleteDraft, 30);
			btnDeleteDraft.click();
			return false;
		}
	}

	public boolean verifyQRCodePrint() {
		ARCFNAAppUtils.waitForElement(driver, btnQRPrintCancel, 30);
		if (btnQRPrintCancel.isDisplayed() == true) {
			btnQRPrintCancel.click();
			return true;
		} else {
			btnQRPrintCancel.click();
			return false;
		}
	}

	public void closeQRWindow() {
		ARCFNAAppUtils.waitForElement(driver, btnCloseQRWindow, 30);
		btnCloseQRWindow.click();
	}

	public void closeEquipOpenWindow() {
		ARCFNAAppUtils.waitForElement(driver, btnEquiSaveBack, 30);
		btnEquiSaveBack.click();
		ARCFNAAppUtils.waitTill(4000);
	}

	public void closeSearchResultwindow() {
		ARCFNAAppUtils.waitForElement(driver, btnEquiSearchBack, 30);
		btnEquiSearchBack.click();
	}

	public void btnMenu() {
		ARCFNAAppUtils.waitForElement(driver, btnMenu, 30);
		btnMenu.click();
		ARCFNAAppUtils.waitTill(5000);
		Log.message("Menu button has been clicked");
	}

	public void logOff() {

		/*
		 * ScreenOrientation orientation= driver.getOrientation(); String orentValue =
		 * orientation.value(); Log.message(orentValue);
		 */
		ARCFNAAppUtils.waitForElement(driver, btnSignOut, 40);
		btnSignOut.click();
		ARCFNAAppUtils.waitTill(6000);
		driver.switchTo().alert().accept();
		Log.message("Successfully Logout");
		ARCFNAAppUtils.waitTill(3000);
		Log.message("Loggin off successfully");
	}

	public void clickAddPhotoVideo() {
		ARCFNAAppUtils.waitForElement(driver, btnAddPhotoEquipment, 20);
		btnAddPhotoEquipment.click();

		int okCount = btnOKPhotoAccessList.size();
		if (okCount > 0) {
			btnOKPhotoAccess.click();
			ARCFNAAppUtils.waitTill(1000);
			btnOKPhotoAccess.click();
			ARCFNAAppUtils.waitTill(1000);
			btnOKPhotoAccess.click();
		}
	}

	public void selectPhotoFromGallary(int noFilesToSelect) {
		ARCFNAAppUtils.waitForElement(driver, btnGallery, 20);
		btnGallery.click();
		ARCFNAAppUtils.waitForElement(driver, btnCameraRoll, 20);
		btnCameraRoll.click();
		int photoFileSize = btnPhotoFiles.size();
		if (photoFileSize > 0 && noFilesToSelect <= photoFileSize) {
			for (int i = 0; i <= noFilesToSelect; i++) {
				ARCFNAAppUtils.waitForElement(driver, btnPhotoFile, 20);
				btnPhotoFile.click();
			}

		}
	}

}
