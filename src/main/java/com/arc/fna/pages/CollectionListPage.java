package com.arc.fna.pages;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;
import org.w3c.dom.Document;

import com.arc.fna.utils.ARCFNAAppUtils;
import com.arcautoframe.utils.Log;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.pagefactory.iOSFindBys;

public class CollectionListPage  extends LoadableComponent<CollectionListPage>{
	
	public static AppiumDriver<MobileElement> driver;
	private String cachedPageSource = null;
	private Document cachedDocument = null;
	private boolean isPageLoaded;
	
	@iOSFindBy(accessibility="TestAutomationSubFolder")
	WebElement cloudSubFolder;
	
	@iOSFindBy(accessibility="TestAutomationFolder")
	WebElement cloudFolder;
	
	@iOSFindBy(accessibility="TestAutomationFolder1")
	WebElement cloudFolder1;
	
	
	@iOSFindBy(accessibility="CollectionNameChanged")
	WebElement collectionNameChanged;
	
	@iOSFindBy(accessibility="Skip")
	List <WebElement> btnSkipsAfterFolder;
	
	@iOSFindBy(accessibility="Skip")
	WebElement btnSkipAfterFolder;
	
	@iOSFindBy(accessibility="skip")
	List <WebElement> btnSkipsAfterFolder1;
	
	@iOSFindBy(accessibility="skip")
	WebElement btnSkipAfterFolder1;
	
	@iOSFindBy(accessibility = "Release PDF.PDF")
	WebElement uploadedFile;
	
	@iOSFindBy(accessibility = "Release PDF.PDF")
	WebElement movedFile;
	
	@iOSFindBy(accessibility = "Release PDF.PDF")
	List<WebElement> uploadedFiles;
	
	@iOSFindBy(accessibility = "com.ARC.PlanwellCollaboration")
	WebElement fnaButtonWifiOff;
	
	@iOSFindBy(accessibility = "Sub Dynamic Folder Testing")
	WebElement cloudSubStaticFolder;
	
	@iOSFindBy(accessibility = "FolderCreated")
	WebElement cloudStaticFolder;
	
	@iOSFindBy(accessibility = "Automation_Testing5")
	WebElement cloudDynamicFolder;
	
	@iOSFindBy(accessibility = "Automation_Testing4")
	WebElement cloudDynamicFolder3;
	
	@iOSFindBy(accessibility = "Automation_Testing6")
	WebElement cloudDynamicFolder6;
	
	@iOSFindBy(accessibility = "Automation Testing - Subhendu")
	WebElement cloudStaticProject;
	
	@iOSFindBy(accessibility = "Sample Facility- Technology Center")
	WebElement cloudDynamicProject;
	
	@iOSFindBy(accessibility = "TestAutomationProject")
	WebElement cloudProject;
	
	@iOSFindBy(accessibility = "com.ARC.PlanwellCollaboration")
	WebElement btnSwitchCollectionWindow;
	
	@iOSFindBy(accessibility = "Facilities & Archive")
	List <WebElement> btnSwitchCollectionWindows;
	
	@iOSFindBy(accessibility = "wifi-button")
	WebElement btnWifi;
	
	@iOSFindBy(accessibility="FileListfileName")
	WebElement folderInsideCollection;
	
	@iOSFindBy(accessibility="ProjectDetailDetailBtn")
	WebElement btnOwnerDetails;
	
	@iOSFindBy(accessibility="ProjectDetailCloseButton")
	WebElement btnProjectInfoClose;
	
	@iOSFindBy(accessibility="ProjectListInfoButton")
	WebElement btnProjectInfo;
	
	@iOSFindBy(accessibility="PRojectListProjectQwnerName")
	WebElement labelProjectCreatedbY;
	
	@iOSFindBy(accessibility="projectCollection")
	MobileElement collectionListAllProjects;
	
	@iOSFindBy(accessibility="ProjectListProjectUnfavoriteButton")
	WebElement btnUnfavourite;
	
	@iOSFindBy(accessibility="ProjectListProjectUnfavoriteButton")
	List<WebElement> btnUnfavourites;
	
	@iOSFindBy(accessibility="ProjectListProjectfavoriteButton")
	List<WebElement> btnFavorites;
	
	@iOSFindBy(accessibility="ProjectListProjectfavoriteButton")
	WebElement btnFavorite;
	
	@iOSFindBy(accessibility="Favorite")
	WebElement tabFavorite;
	
	@iOSFindBy(accessibility="All")
	MobileElement tabAll;
	
	@iOSFindBy(accessibility="Pending")
	WebElement tabPending;
	
	@iOSFindBy(accessibility="DrawerControlBtn")
	WebElement btnMenu;
	
	@iOSFindBy(accessibility="SignOutMenuBtn")
	@CacheLookup
	WebElement btnSignOut;
	
	@iOSFindBy(accessibility="Sample Facility- Technology Center")
	WebElement btnProject;
	
	@iOSFindBy(accessibility="Automation_Testing_Use_Only")
	WebElement btnProject1;
	
	@iOSFindBy(accessibility="TestAutomationProject")
	WebElement btnTestAutomationProject;
	
	@iOSFindBy(accessibility="skip")
	WebElement btnSkip;
	
	@iOSFindBy(accessibility="skip")
	List <WebElement> btnSkips;
	
	@iOSFindBy(xpath="//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSwitch")
	WebElement btnFolderSwitch;
	
	
	@iOSFindBy(accessibility="Album")
	WebElement menuAlbum;
	
	@iOSFindBy(accessibility="Album")
	List<WebElement> menuAlbumPresent;
	
	@iOSFindBy(accessibility="HeaderTitleLbl")
	WebElement lblCollectionList;
	
	@iOSFindBy(accessibility="MenuTitle")
	WebElement btnCollectionList;

	@Override
	protected void load() {
		isPageLoaded = true;
		ARCFNAAppUtils.waitForElement(driver, btnMenu);
	}
	
	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}
	
	public CollectionListPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		//PageFactory.initElements(this.driver, this);
	}
	
	public void menuButtonClick()
	{
		ARCFNAAppUtils.waitForElement(driver, btnMenu, 20);
		Log.message("Clicking on Menu button");
		btnMenu.click();
	}
	
	public void collectionListClick()
	{
		ARCFNAAppUtils.waitForElement(driver, lblCollectionList, 20);
		lblCollectionList.click();
	}
	
	public AlbumPage albumButtonClick()  throws IOException, InterruptedException
	{
		Log.message("Clicking on Album button");
		ARCFNAAppUtils.waitForElement(driver, menuAlbum, 40);
		menuAlbum.click();
		
		/*TouchAction action1 = new TouchAction(driver);
		action1.press(menuAlbum).release().perform();
		action1.release();*/
		
		return new AlbumPage(driver).get();
	}
	
	public boolean presenceOfAlbumMenu()
	{
		ARCFNAAppUtils.waitForListElement(driver, menuAlbumPresent, 30);
		if(menuAlbumPresent.size()>0)
			return true;
			else
			return false;
		
	}
	
	public boolean nonPresenceOfAlbumMenu()
	{
		Log.message("Checking Album menu present or not?");
		ARCFNAAppUtils.waitForListElement(driver, menuAlbumPresent, 20);
		if(menuAlbumPresent.size()<1)
			return true;
			else
			return false;
		
	}
	
	public boolean presenceOfMenuButton()
	{
		ARCFNAAppUtils.waitForElement(driver, btnMenu, 40);
		Log.message("Checking whether Menu button is appeared?");
		if(btnMenu.isDisplayed())
			return true;
			else
			return false;
	}
	
	public void logOff() {

		/*
		 * ScreenOrientation orientation= driver.getOrientation(); String orentValue =
		 * orientation.value(); Log.message(orentValue);
		 */
		ARCFNAAppUtils.waitForElement(driver, btnMenu, 40);
		btnMenu.click();
		Log.message("Menu button clicked");
		ARCFNAAppUtils.waitForElement(driver, btnSignOut, 40);
		btnSignOut.click();
		ARCFNAAppUtils.waitTill(6000);
		driver.switchTo().alert().accept();
		Log.message("Successfully Logout");
		ARCFNAAppUtils.waitTill(3000);
		Log.message("Loggin off successfully");
	}
	
	/*public void logOff() throws InterruptedException
	{
		ARCFNAAppUtils.waitForElement(driver, btnMenu, 40);
		btnMenu.click();
		Log.message("Menu button clicked");
		//Thread.sleep(1000);
		ARCFNAAppUtils.waitForElement(driver, btnLogOut, 20);
		btnLogOut.click();
		//Thread.sleep(2000);
		driver.switchTo().alert().accept();
		Log.message("Successfully Logout");
		ARCFNAAppUtils.waitTill(3000);
	}*/
	
	public FoldersNFilesPage openProject() throws InterruptedException
	{
		ARCFNAAppUtils.waitForElement(driver, btnProject, 30);
		btnProject.click();
		Thread.sleep(2000);
		ARCFNAAppUtils.waitForElement(driver, btnSkip, 50);
		int noSkipBtn = btnSkips.size();
		if(noSkipBtn>0)
		{
		btnSkip.click();
		}
		Log.message("Desired collection / Project is opened");
		return new FoldersNFilesPage(driver).get();	
	}
	
	public FoldersNFilesPage openProjectWithoutSkip() throws InterruptedException
	{
		/*ARCFNAAppUtils.waitForElement(driver, btnFolderSwitch, 50);
		btnFolderSwitch.click();
		Thread.sleep(20000);*/
		//Log.message("Successfully Logout");
		ARCFNAAppUtils.waitForElement(driver, btnProject, 30);
		btnProject.click();
		Thread.sleep(2000);
		Log.message("Desired collection / Project is opened");
		return new FoldersNFilesPage(driver).get();	
	}
	
	public void openProject1() throws InterruptedException
	{
		/*ARCFNAAppUtils.waitForElement(driver, btnFolderSwitch, 50);
		btnFolderSwitch.click();
		Thread.sleep(20000);*/
		//Log.message("Successfully Logout");
		ARCFNAAppUtils.waitForElement(driver, btnProject1, 30);
		btnProject1.click();
		Thread.sleep(2000);
		ARCFNAAppUtils.waitForElement(driver, btnSkip, 20);
		int noSkipBtn = btnSkips.size();
		if(noSkipBtn>0)
		{
		btnSkip.click();
		}
		//Thread.sleep(25000);
	}
	
	public FoldersNFilesPage openTestAutomationProject() throws InterruptedException
	{
		Log.message("Opening desired project");
		ARCFNAAppUtils.waitForElement(driver, btnTestAutomationProject, 30);
		btnTestAutomationProject.click();
		Thread.sleep(2000);
		ARCFNAAppUtils.waitForElement(driver, btnSkip, 20);
		int noSkipBtn = btnSkips.size();
		if(noSkipBtn>0)
		{
		btnSkip.click();
		}
		return new FoldersNFilesPage(driver).get();
	}
	
	public void clickProject() throws InterruptedException
	{
		ARCFNAAppUtils.waitForElement(driver, btnProject, 20);
		btnProject.click();
		Thread.sleep(2000);
	}
	
	public boolean presenceOfCollectionList()
	{
		ARCFNAAppUtils.waitForElement(driver, lblCollectionList, 30);
		if(lblCollectionList.isDisplayed() && lblCollectionList.getText().equalsIgnoreCase("Collection list"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void openCollectionList()
	{
		ARCFNAAppUtils.waitForElement(driver, btnMenu, 30);
		btnMenu.click();
		ARCFNAAppUtils.waitForElement(driver, btnCollectionList, 30);
		btnCollectionList.click();
	}
	
	@SuppressWarnings("unused")
	public boolean btnAllEnabled()
	{
		ARCFNAAppUtils.waitForElement(driver, tabAll, 30);
		ARCFNAAppUtils.waitForElement(driver, btnUnfavourite, 30);
		int cntProject = collectionListAllProjects.findElements(By.name("PRojectListProjectQwnerName")).size();
		int fabProjectCount = collectionListAllProjects.findElements(By.name("ProjectListProjectfavoriteButton")).size();
		
		Log.message("Project Count:"+" "+cntProject);
		Log.message("Favourite Project Count:"+" "+fabProjectCount);
		Log.message("Value of TABAll button:"+" "+tabAll.getAttribute("value"));
		
		
		for(int i = 0; i<=cntProject; i++)
		{
		if(tabAll.getAttribute("value") != null &&  fabProjectCount<1 )
		{
			return true;
		}
		else
		{
			return false;
		}
		}
		return true;
		
	}
	
	public boolean btnFavoriteEnabled()
	{
		ARCFNAAppUtils.waitForElement(driver, tabFavorite, 30);
		ARCFNAAppUtils.waitForElement(driver, btnFavorite, 30);
		if(tabFavorite.getAttribute("value") != null &&  btnFavorites.size()>0 )
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	@SuppressWarnings("unused")
	public void clickUnFavorite()
	{
		ARCFNAAppUtils.waitForElement(driver, collectionListAllProjects, 30);
		int noOfUnfav = collectionListAllProjects.findElements(By.name("ProjectListProjectUnfavoriteButton")).size();
		Log.message("Unfav button count"+" "+noOfUnfav);
		for (int i=0; i<=noOfUnfav; i++)
		{
			ARCFNAAppUtils.waitForElement(driver, btnUnfavourite, 20);
			//btnUnfavourite.click();
			TouchAction action1 = new TouchAction(driver);
			action1.press(btnUnfavourite).release().perform();
			action1.release();
			break;
		}
		
	}
	
	@SuppressWarnings("unused")
	public void clickFavorite()
	{
		ARCFNAAppUtils.waitForElement(driver, btnFavorite, 30);
		//btnFavorite.click();
		TouchAction action1 = new TouchAction(driver);
		action1.tap(btnFavorite).release().perform();
		action1.release();
		//ARCFNAAppUtils.waitForElement(driver, btnMenu, 20);
	}
	
	public boolean SaveProjectInfo()
	{
		ARCFNAAppUtils.waitForElement(driver, collectionListAllProjects, 30);
		int noOfUnfav = collectionListAllProjects.findElements(By.name("ProjectListProjectUnfavoriteButton")).size();
		Log.message("Unfav button count"+" "+noOfUnfav);
		for (int i=0; i<=noOfUnfav; i++)
		{
			ARCFNAAppUtils.waitForElement(driver, btnProjectInfo, 20);
			btnProjectInfo.click();
			ARCFNAAppUtils.waitForElement(driver, btnProjectInfoClose, 20);
			ARCFNAAppUtils.waitForElement(driver, btnOwnerDetails, 10);
			if(btnProjectInfoClose.isEnabled() && btnOwnerDetails.isEnabled())
			{
				TouchAction action5 = new TouchAction(driver);
				action5.press(btnProjectInfoClose).release().perform();
				return true;
			}
			else
			{
				return false;
			}
			
		}
		return true;
	}
	
	@SuppressWarnings("unused")
	public void offWiFi()
	{
		try
		{
			//((MobileDriver) driver).swipe(509,761, 625, 285, 1000);
			
			// 962, 0, 962, 228
			
			TouchAction t1 = new TouchAction(driver);
			t1.longPress(962, 0).moveTo(962, 228).perform().release();
			ARCFNAAppUtils.waitTill(2000);
			
			ARCFNAAppUtils.waitForElement(driver, btnWifi, 30);
			if(btnWifi.getAttribute("value").equals("1"))
			{
			btnWifi.click();
			}
			t1.cancel();
				
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void clickFNA()
	{
		ARCFNAAppUtils.waitTill(4000);
		TouchAction t2 = new TouchAction(driver);
		t2.press(389, 433).perform().release();
		t2.cancel();
		//ARCFNAAppUtils.waitForElement(driver, fnaButtonWifiOff, 20);
		//fnaButtonWifiOff.click();
	}
	
	
	public boolean verifyOpeningProjectWhenWifiOffNoFileSynced()
	{
		String expectedString = "Unable to connect to SKYSITE. Please check your internet connection.";
		String actualString = driver.switchTo().alert().getText();
		if(expectedString.equalsIgnoreCase(actualString))
		{
			driver.switchTo().alert().accept();
			return true;
		}
		else
		{
			driver.switchTo().alert().accept();
			return false;
		}
		
	}
	
	@SuppressWarnings("unused")
	public void onWiFi()
	{
		try
		{
			//((MobileDriver) driver).swipe(509,761, 625, 285, 1000);
			TouchAction t1 = new TouchAction(driver);
			t1.longPress(962, 0).moveTo(962, 228).perform().release();
			ARCFNAAppUtils.waitTill(2000);
			
			ARCFNAAppUtils.waitForElement(driver, btnWifi, 30);
			if(btnWifi.getAttribute("value").equals("0"))
			{
			btnWifi.click();
			}
			ARCFNAAppUtils.waitTill(3000);
			t1.cancel();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unused")
	public void onWiFiForceFully()
	{
		try
		{
			((MobileDriver) driver).swipe(509,761, 625, 285, 1000);
			
			ARCFNAAppUtils.waitForElement(driver, btnWifi, 30);
			if(btnWifi.getAttribute("value").equals("0"))
			{
			btnWifi.click();
			ARCFNAAppUtils.waitTill(2000);
			ARCFNAAppUtils.waitForElement(driver, fnaButtonWifiOff, 20);
			fnaButtonWifiOff.click();
			}
			
			
			ARCFNAAppUtils.waitTill(3000);
				
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public boolean verifyPresenceProjectCloud()
	{
		//Log.message("FNA App login done.");
		ARCFNAAppUtils.waitForElement(driver, cloudProject, 20);
		if(cloudProject.isDisplayed())
		{
			Log.message("Project is visible in DEVICE / MOBILE that has been created through FNA Web Application");
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	public boolean verifyPresenceFolder()
	{
		ARCFNAAppUtils.waitForElement(driver, cloudProject, 20);
		cloudProject.click();
		ARCFNAAppUtils.waitForElement(driver, btnSkip, 20);
		btnSkip.click();
		ARCFNAAppUtils.waitForElement(driver, cloudFolder, 20);
		if(cloudFolder.isDisplayed())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean verifyPresenceSubFolder()
	{
		ARCFNAAppUtils.waitForElement(driver, cloudProject, 20);
		cloudProject.click();
		ARCFNAAppUtils.waitForElement(driver, btnSkip, 20);
		btnSkip.click();
		ARCFNAAppUtils.waitForElement(driver, cloudFolder, 20);
		cloudFolder.click();
		ARCFNAAppUtils.waitForElement(driver, cloudSubFolder, 20);
		if(cloudSubFolder.isDisplayed())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean verifyPresenceOfFile()
	{
		ARCFNAAppUtils.waitForElement(driver, cloudProject, 20);
		cloudProject.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnSkip, 25);
		int noSkipBtn1 = btnSkips.size();
		if(noSkipBtn1>0)
		{
		btnSkip.click();
		}
		
		ARCFNAAppUtils.waitForElement(driver, cloudFolder, 20);
		cloudFolder.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnSkipAfterFolder, 25);
		int noSkipBtn = btnSkipsAfterFolder.size();
		if(noSkipBtn>0)
		{
			btnSkipAfterFolder.click();
		}
		
		ARCFNAAppUtils.waitForElement(driver, uploadedFile, 20);
		if(uploadedFile.isDisplayed())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean verifyPresenceOfFileAfterMove()
	{
		ARCFNAAppUtils.waitForElement(driver, cloudProject, 20);
		cloudProject.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnSkip, 25);
		int noSkipBtn1 = btnSkips.size();
		if(noSkipBtn1>0)
		{
		btnSkip.click();
		}
		
		ARCFNAAppUtils.waitForElement(driver, cloudFolder1, 20);
		cloudFolder1.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnSkipAfterFolder, 25);
		int noSkipBtn = btnSkipsAfterFolder.size();
		if(noSkipBtn>0)
		{
			btnSkipAfterFolder.click();
		}
		
		ARCFNAAppUtils.waitForElement(driver, movedFile, 20);
		if(movedFile.isDisplayed())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean verifyDeletionOfFile()
	{
		ARCFNAAppUtils.waitForElement(driver, cloudProject, 20);
		cloudProject.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnSkip, 25);
		int noSkipBtn1 = btnSkips.size();
		if(noSkipBtn1>0)
		{
		btnSkip.click();
		}
		
		ARCFNAAppUtils.waitForElement(driver, cloudFolder, 20);
		cloudFolder.click();
		
		ARCFNAAppUtils.waitForElement(driver, btnSkipAfterFolder, 25);
		int noSkipBtn = btnSkipsAfterFolder.size();
		if(noSkipBtn>0)
		{
			btnSkipAfterFolder.click();
		}
		
		ARCFNAAppUtils.waitForElement(driver, uploadedFile, 20);
		if(uploadedFiles.size()<1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean verifyProjectNameChangeFromWeb() 
	{
		ARCFNAAppUtils.waitForElement(driver, collectionNameChanged, 10);
		if(collectionNameChanged.isDisplayed())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void syncWindow()
	{
		TouchAction t5 = new TouchAction(driver);
		t5.longPress(459, 374).moveTo(459, 727).perform().release();
		ARCFNAAppUtils.waitTill(4000);
		Log.message("Chnage Set has been taken successfully in DEVICE");
	}
	
	public void openProject_AutomationTestingUseOnly()
	{
		ARCFNAAppUtils.waitForElement(driver, btnProject1, 20);
		btnProject1.click();
		ARCFNAAppUtils.waitForElement(driver, btnSkip, 25);
		int noSkipBtn1 = btnSkips.size();
		if(noSkipBtn1>0)
		{
		btnSkip.click();
		}
		Log.message("Desired Collection / Project opened successfully");
	}
	
	public boolean verifyPartialChangeSet()
	{
		
		ARCFNAAppUtils.waitForElement(driver, cloudFolder, 20);
		if(cloudFolder.isDisplayed())
		{
			cloudFolder.click();
			ARCFNAAppUtils.waitForElement(driver, btnSkipAfterFolder, 25);
			int noSkipBtn = btnSkipsAfterFolder.size();
			if(noSkipBtn>0)
			{
				btnSkipAfterFolder.click();
			}
			ARCFNAAppUtils.waitForElement(driver, uploadedFile, 20);
			if(uploadedFile.isDisplayed())
			{
				return true;
			}
		}
		else
		{
			return false;
		}
		
		return false;
	}
	

}
