package com.arc.fna.tests;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.arc.fna.pages.CloudActivityPage;
import com.arc.fna.pages.CollectionListPage;
import com.arc.fna.pages.FNALoginPage;
import com.arc.fna.utils.ARCFNAAppUtils;
import com.arc.fna.utils.StartStopAppiumServer;
import com.arcautoframe.utils.AppiumDriverFactory;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;
import com.arcautoframe.utils.RetryAnalyzer;
import com.arcautoframe.utils.WebDriverFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

@Listeners(EmailReport.class)

public class FNA_Integration {
	
	FNALoginPage loginPage;
	static AppiumDriver<MobileElement> driver;
	CollectionListPage collectionListPage;
	public WebDriver cloudDriver;
	
	@BeforeClass
	public void beforeClass() throws InterruptedException, IOException, AWTException
	{
		driver = AppiumDriverFactory.get();
		Log.message("FNA SKYSITE Mobile app is launched successfully");
		 //StartStopAppiumServer.startAppiumServer();
		
	}
	
	@Test(priority = 1, description = "TC_001: Verify Project creation through Web / cloud is reflected in Device.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	public void verifyProjectCreationWebDevice() throws Exception
	{
		Log.testCaseInfo("TC_001: Verify Project creation from Web / cloud is reflected in Device.");
		try
		{	
		cloudDriver = ARCFNAAppUtils.browserAppLaunch("chrome", ARCFNAAppUtils.testDataReading("Staging")); /* Launching chrome browser */
		CloudActivityPage cloudactivitypage = PageFactory.initElements(cloudDriver,CloudActivityPage.class);
		cloudactivitypage.loginWeb(); /* Login into FNA */
		cloudactivitypage.deleteCollection(); /* Deleting existing collection named as "TestAutomationProject", which used last time for this test case*/
		cloudactivitypage.createCollection(); /* Creating Collection named as "TestAutomationProject"*/
		loginPage = new FNALoginPage(driver).get();
		collectionListPage = loginPage.loginWithValidCredential(); /* Login into FNA through device */
		Log.assertThat(collectionListPage.verifyPresenceProjectCloud(), "Projects is visible has been created through cloud", "Projects is not visible has been created through cloud", driver);
		collectionListPage.logOff(); /* Login off through Device */
		
		}
		catch(Exception e) {
			driver.launchApp();
			cloudDriver.quit();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			cloudDriver.quit();
		}
		
	}
	
	
	@Test(priority = 2, description = "TC_002: Verify Folder creation through Web is reflected in Device.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	public void verifyFolderCreationWebDevice() throws Exception
	{
		Log.testCaseInfo("TC_002: Verify Folder creation from Web is reflected in Device.");
		try
		{
		cloudDriver = ARCFNAAppUtils.browserAppLaunch("chrome", ARCFNAAppUtils.testDataReading("Production")); /* Launching chrome browser */
		CloudActivityPage cloudactivitypage = PageFactory.initElements(cloudDriver,CloudActivityPage.class);
		cloudactivitypage.loginWeb(); /* Login into FNA */
		cloudactivitypage.deleteCollection(); /* Deleting existing collection named as "TestAutomationProject", which used last time for this test case*/
		cloudactivitypage.createCollection(); /* Creating Collection named as "TestAutomationProject"*/
		cloudactivitypage.createFolder(); /* Creating Folder named as "TestAutomationFolder" */
		loginPage = new FNALoginPage(driver).get();
		collectionListPage = loginPage.loginWithValidCredential(); /* Login into FNA through device */
		Log.assertThat(collectionListPage.verifyPresenceFolder(), "Folder is visible has been created through cloud", "Folder is not visible has been created through cloud", driver);
		collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			cloudDriver.quit();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			cloudDriver.quit();
		}
	}
		
		@Test(priority = 3, description = "TC_003: Verify Sub Folder creation through Web is reflected in Device.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
		public void verifySubFolderCreationWebDevice() throws Exception
		{
			Log.testCaseInfo("TC_003: Verify Sub Folder creation from Web is reflected in Device.");
			try
			{
			cloudDriver = ARCFNAAppUtils.browserAppLaunch("chrome", ARCFNAAppUtils.testDataReading("Production"));
			CloudActivityPage cloudactivitypage = PageFactory.initElements(cloudDriver,CloudActivityPage.class);
			cloudactivitypage.loginWeb();	
			cloudactivitypage.deleteCollection(); /* Deleting existing collection named as "TestAutomationProject", which used last time for this test case*/
			cloudactivitypage.createCollection(); /* Creating Collection named as "TestAutomationProject"*/
			cloudactivitypage.createFolder(); /* Creating Folder named as "TestAutomationFolder" */
			cloudactivitypage.createSubFolder(); /* Creating Sub Folder named as "TestAutomationSubFolder" */
			loginPage = new FNALoginPage(driver).get();
			collectionListPage = loginPage.loginWithValidCredential(); /* Login into FNA through device */
			Log.assertThat(collectionListPage.verifyPresenceSubFolder(), "Sub Folder is visible has been created through cloud", "Sub Folder is not visible has been created through cloud", driver);
			collectionListPage.logOff();
			
			}
			catch(Exception e) {
				driver.launchApp();
				cloudDriver.quit();
				e.getCause();
				Log.exception(e, driver);
			}
			finally
			{
				Log.endTestCase();
				cloudDriver.quit();
			}
		
	}
		
		@Test(priority = 4, description = "TC_004: Verify file upload through cloud, is reflected in Device.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
		public void verifyFileUploadWebDevice() throws Exception
		{
			Log.testCaseInfo("TC_004: Verify file upload is reflected in Device.");
			try
			{
			cloudDriver = ARCFNAAppUtils.browserAppLaunch("chrome", ARCFNAAppUtils.testDataReading("Production"));
			CloudActivityPage cloudactivitypage = PageFactory.initElements(cloudDriver,CloudActivityPage.class);
			cloudactivitypage.loginWeb();	
			cloudactivitypage.deleteCollection(); /* Deleting existing collection named as "TestAutomationProject", which used last time for this test case*/
			cloudactivitypage.createCollection(); /* Creating Collection named as "TestAutomationProject"*/
			cloudactivitypage.createFolder(); /* Creating Folder named as "TestAutomationFolder" */
			cloudactivitypage.uploadFileInitiate(); /* Clicking on Upload Files */
			loginPage = new FNALoginPage(driver).get();
			collectionListPage = loginPage.loginWithValidCredential(); /* Login into FNA through device */
			Log.assertThat(collectionListPage.verifyPresenceOfFile(), "File is visible has been uploaded through cloud", "File is not visible has been uploaded through cloud", driver);
			collectionListPage.logOff();
			
			}
			catch(Exception e) {
				driver.launchApp();
				cloudDriver.quit();
				e.getCause();
				Log.exception(e, driver);
			}
			finally
			{
				Log.endTestCase();
				cloudDriver.quit();
			}
		
	}
		
		@Test(priority = 5, description = "TC_005: Verify file deletion through cloud, is reflected in Device.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
		public void verifyFileDeleteWebDevice() throws Exception
		{
			Log.testCaseInfo("TC_005: Verify file deletion from Web is reflected in Device.");
			try
			{
			cloudDriver = ARCFNAAppUtils.browserAppLaunch("chrome", ARCFNAAppUtils.testDataReading("Production"));
			CloudActivityPage cloudactivitypage = PageFactory.initElements(cloudDriver,CloudActivityPage.class);
			cloudactivitypage.loginWeb();	
			cloudactivitypage.deleteCollection(); /* Deleting existing collection named as "TestAutomationProject", which used last time for this test case*/
			cloudactivitypage.createCollection(); /* Creating Collection named as "TestAutomationProject"*/
			cloudactivitypage.createFolder(); /* Creating Folder named as "TestAutomationFolder" */
			cloudactivitypage.uploadFileInitiate(); /* Clicking on Upload Files */
			cloudactivitypage.deleteFile(); /* Deleting File */
			loginPage = new FNALoginPage(driver).get();
			collectionListPage = loginPage.loginWithValidCredential(); /* Login into FNA through device */
			Log.assertThat(collectionListPage.verifyDeletionOfFile(), "File is not visible after deletion through cloud", "File is visible after deletion through cloud", driver);
			collectionListPage.logOff();
			}
			catch(Exception e) {
				driver.launchApp();
				cloudDriver.quit();
				e.getCause();
				Log.exception(e, driver);
			}
			finally
			{
				Log.endTestCase();
				cloudDriver.quit();
			}
		
	}
		
		@Test(priority = 6, description = "TC_006: Verify File move from Web is reflected in Device.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
		public void verifyFileMoveWebDevice() throws Exception
		{
			Log.testCaseInfo("TC_006: Verify File move from Web is reflected in Device.");
			try
			{
			cloudDriver = ARCFNAAppUtils.browserAppLaunch("chrome", ARCFNAAppUtils.testDataReading("Production"));
			CloudActivityPage cloudactivitypage = PageFactory.initElements(cloudDriver,CloudActivityPage.class);
			cloudactivitypage.loginWeb();	
			cloudactivitypage.deleteCollection(); /* Deleting existing collection named as "TestAutomationProject", which used last time for this test case*/
			cloudactivitypage.createCollection(); /* Creating Collection named as "TestAutomationProject"*/
			cloudactivitypage.createFolder(); /* Creating Folder named as "TestAutomationFolder" */
			cloudactivitypage.uploadFileInitiate();
			cloudactivitypage.create2ndFolderUnderproject();
			cloudactivitypage.fileMove();
			loginPage = new FNALoginPage(driver).get();
			collectionListPage = loginPage.loginWithValidCredential();
			Log.assertThat(collectionListPage.verifyPresenceOfFileAfterMove(), "File is visible after moved through cloud", "File is not visible after moved through cloud", driver);
			collectionListPage.logOff();
			}
			catch(Exception e) {
				driver.launchApp();
				cloudDriver.quit();
				e.getCause();
				Log.exception(e, driver);
			}
			finally
			{
				Log.endTestCase();
				cloudDriver.quit();
			}
		
	}
		
		@Test(priority = 7, description = "TC_007: Verify File copy from Web is reflected in Device.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
		public void verifyFileCopyWebDevice() throws Exception
		{
			Log.testCaseInfo("TC_007: Verify File copy from Web is reflected in Device.");
			try
			{
			cloudDriver = ARCFNAAppUtils.browserAppLaunch("chrome", ARCFNAAppUtils.testDataReading("Production"));
			CloudActivityPage cloudactivitypage = PageFactory.initElements(cloudDriver,CloudActivityPage.class);
			cloudactivitypage.loginWeb();	
			cloudactivitypage.deleteCollection(); /* Deleting existing collection named as "TestAutomationProject", which used last time for this test case*/
			cloudactivitypage.createCollection(); /* Creating Collection named as "TestAutomationProject"*/
			cloudactivitypage.createFolder(); /* Creating Folder named as "TestAutomationFolder" */
			cloudactivitypage.uploadFileInitiate();
			cloudactivitypage.create2ndFolderUnderproject();
			cloudactivitypage.fileCopy();
			loginPage = new FNALoginPage(driver).get();
			collectionListPage = loginPage.loginWithValidCredential();
			Log.assertThat(collectionListPage.verifyPresenceOfFileAfterMove(), "File is visible after copied through cloud", "File is not visible after copied through cloud", driver);
			collectionListPage.logOff();
			}
			catch(Exception e) {
				driver.launchApp();
				cloudDriver.quit();
				e.getCause();
				Log.exception(e, driver);
			}
			finally
			{
				Log.endTestCase();
				cloudDriver.quit();
			}
		}
		
		@Test(priority = 8, description = "TC_010: Verify Collection name modification from Web is reflected in Device.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
		public void verifyCollecitonNameModificationWebDevice() throws Exception
		{
			Log.testCaseInfo("TC_010: Verify Collection name modification from Web is reflected in Device.");
			try
			{
			cloudDriver = ARCFNAAppUtils.browserAppLaunch("chrome", ARCFNAAppUtils.testDataReading("Production"));
			CloudActivityPage cloudactivitypage = PageFactory.initElements(cloudDriver,CloudActivityPage.class);
			cloudactivitypage.loginWeb();	
			cloudactivitypage.deleteCollection(); /* Deleting existing collection named as "TestAutomationProject", which used last time for this test case*/
			cloudactivitypage.createCollection(); /* Creating Collection named as "TestAutomationProject"*/
			cloudactivitypage.collectionNameChange();
			loginPage = new FNALoginPage(driver).get();
			collectionListPage = loginPage.loginWithValidCredential();
			Log.assertThat(collectionListPage.verifyProjectNameChangeFromWeb(), "Collection changed name from web is reflected in device", "Collection changed name from web is not reflected in device");
			collectionListPage.logOff();
			cloudactivitypage.deleteNameChangedCollection();
			}
			catch(Exception e) {
				driver.launchApp();
				cloudDriver.quit();
				e.getCause();
				Log.exception(e, driver);
			}
			finally
			{
				Log.endTestCase();
				cloudDriver.quit();
			}
		}
		
		@Test(priority = 9, description = "TC_012: Verify full PROJECT Sync in device through Project Dashboard.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
		public void verifyProjectSync() throws Exception
		{
			Log.testCaseInfo("TC_012: Verify full PROJECT Sync in device through Project Dashboard.");
			try
			{
			loginPage = new FNALoginPage(driver).get();
			collectionListPage = loginPage.loginWithValidCredential();
			cloudDriver = ARCFNAAppUtils.browserAppLaunch("chrome", ARCFNAAppUtils.testDataReading("Production"));
			Log.message("SKYSITE FNA launched successfully in CHROME browser");
			CloudActivityPage cloudactivitypage = PageFactory.initElements(cloudDriver,CloudActivityPage.class);
			cloudactivitypage.loginWeb();	
			cloudactivitypage.deleteCollection(); /* Deleting existing collection named as "TestAutomationProject", which used last time for this test case*/
			collectionListPage.syncWindow();
			cloudactivitypage.createCollection(); /* Creating Collection named as "TestAutomationProject"*/
			collectionListPage.syncWindow();
			Log.assertThat(collectionListPage.verifyPresenceProjectCloud(), "Projects is visible IN DEVICE / MOBILE that has been created through cloud", "Projects is not visible in DEVICE / MOBILE that has been created through cloud", driver);
			collectionListPage.logOff();
			}
			catch(Exception e) {
				driver.launchApp();
				cloudDriver.quit();
				e.getCause();
				Log.exception(e, driver);
			}
			finally
			{
				Log.endTestCase();
				cloudDriver.quit();
			}
		}
		
		
		@Test(priority = 10, description = "TC_013: Verify partial Project Sync in device.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
		public void verifyPartialProjectSync() throws Exception
		{
			Log.testCaseInfo("TC_013: Verify partial Project Sync in device.");
			try
			{
			loginPage = new FNALoginPage(driver).get();
			collectionListPage = loginPage.loginWithValidCredential();
			collectionListPage.openProject_AutomationTestingUseOnly();
			cloudDriver = ARCFNAAppUtils.browserAppLaunch("chrome", ARCFNAAppUtils.testDataReading("Production"));
			Log.message("SKYSITE FNA launched successfully in CHROME browser");
			CloudActivityPage cloudactivitypage = PageFactory.initElements(cloudDriver,CloudActivityPage.class);
			cloudactivitypage.loginWeb();	
			cloudactivitypage.openProjectAutomationTestingUseOnly();
			cloudactivitypage.deleteFirstFolder();
			collectionListPage.syncWindow();
			cloudactivitypage.createFolder();
			cloudactivitypage.uploadFileInitiate();
			collectionListPage.syncWindow();
			Log.assertThat(collectionListPage.verifyPartialChangeSet(), "Files and Folder are visible in DEVICE / MOBILE, those have been uploaded through CLOUD", "Files and Folder are not visible in DEVICE / MOBILE, those have been uploaded through CLOUD");
			collectionListPage.logOff();
			}
			catch(Exception e) {
				driver.launchApp();
				cloudDriver.quit();
				e.getCause();
				Log.exception(e, driver);
			}
			finally
			{
				Log.endTestCase();
				cloudDriver.quit();
			}
		}
		
		@Test(priority = 11, description = "TC_014: Verify Metadata Project Sync in device.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
		public void verifyMetadataProjectSync() throws Exception
		{
			Log.testCaseInfo("TC_014: Verify Metadata Project Sync in device.");
			try
			{
			cloudDriver = ARCFNAAppUtils.browserAppLaunch("chrome", ARCFNAAppUtils.testDataReading("Production"));
			Log.message("SKYSITE FNA launched successfully in CHROME browser");
			CloudActivityPage cloudactivitypage = PageFactory.initElements(cloudDriver,CloudActivityPage.class);
			cloudactivitypage.loginWeb();	
			cloudactivitypage.openProjectAutomationTestingUseOnly();
			cloudactivitypage.deleteFirstFolder();
			
			
			
			
			
			collectionListPage.syncWindow();
			cloudactivitypage.createFolder();
			cloudactivitypage.uploadFileInitiate();
			collectionListPage.syncWindow();
			Log.assertThat(collectionListPage.verifyPartialChangeSet(), "Files and Folder are visible in DEVICE / MOBILE, those have been uploaded through CLOUD", "Files and Folder are not visible in DEVICE / MOBILE, those have been uploaded through CLOUD");
			collectionListPage.logOff();
			}
			catch(Exception e) {
				driver.launchApp();
				cloudDriver.quit();
				e.getCause();
				Log.exception(e, driver);
			}
			finally
			{
				Log.endTestCase();
				cloudDriver.quit();
			}
		}
		
		
		
		
		
		
		
		
		
		
		// *********************** RND ************************ //
		
		/*@Test(priority = 6, description = "TC_006: Verify file upload is reflected in Device.", enabled = true retryAnalyzer = RetryAnalyzer.class)
		public void verifyTest() throws Exception
		{
			Log.testCaseInfo("TC_004: Verify file upload is reflected in Device.");
			try
			{
			cloudDriver = ARCFNAAppUtils.browserAppLaunch("chrome", ARCFNAAppUtils.testDataReading("Staging"));
			CloudActivityPage cloudactivitypage = PageFactory.initElements(cloudDriver,CloudActivityPage.class);
			cloudactivitypage.loginWeb();	
			cloudactivitypage.openCollectionList();
			cloudactivitypage.searchDynamicCollection();
			cloudactivitypage.openStaticCollection();
			cloudactivitypage.openFolder1();
			cloudactivitypage.openFile1();
			cloudactivitypage.toolSelection();
			cloudactivitypage.markupDrawing();
			
			}
			
			catch(Exception e) {
				driver.launchApp();
				cloudDriver.quit();
				e.getCause();
				Log.exception(e, driver);
			}
			finally
			{
				Log.endTestCase();
				cloudDriver.quit();
			}
		}*/
	
	
		@AfterClass
		public void quit()
		{
			driver.quit();
			cloudDriver.quit();
		}
	

}
