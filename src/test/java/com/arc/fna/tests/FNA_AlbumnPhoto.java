package com.arc.fna.tests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.AlbumPage;
import com.arc.fna.pages.CollectionListPage;
import com.arc.fna.pages.FNALoginPage;
import com.arcautoframe.utils.AppiumDriverFactory;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;
import com.arcautoframe.utils.RetryAnalyzer;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

@Listeners(EmailReport.class)

public class FNA_AlbumnPhoto {
	
	FNALoginPage loginPage;
	CollectionListPage collectionListPage;
	AlbumPage albumPage;
	static AppiumDriver<MobileElement> driver;
	
	@BeforeClass
	public void beforeMethod() throws InterruptedException
	{
		driver = AppiumDriverFactory.get();
		Log.message("FNA app launched successfully");
		Thread.sleep(8000);
		 //locationSelectionPage = new LocationSelectionPage(driver).get();
		//locationSelectionPage.clickDontAllow();
	}
	
	/*@BeforeMethod
	public void onWifiForceFully()
	{
	 collectionListPage.onWiFiForceFully();
	}*/
	
	@Test(priority = 0, description = "TC_016: Verify Album menu would not be displayed without entering a Project.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyUnavailabilityOfAlbumMenu(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_016: Verify Album menu would not be displayed without entering a Project.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			collectionListPage.menuButtonClick();
			Log.assertThat(collectionListPage.nonPresenceOfAlbumMenu(), "Album menu icon is not visible when control is in Collection list page", "Album menu icon is visible when control is in Collection list page",driver);
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			//driver.resetApp();
			//driver.closeApp();
			//collectionListPage.logOff();
			//driver.launchApp();
		}
	}
	
	@Test(priority = 1, description = "TC_017: Verify Album menu would be displayed after entering a Project.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyAvailabilityOfAlbumMenu(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_017: Verify Album menu would be displayed after entering a Project.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			collectionListPage.openProject();
			collectionListPage.menuButtonClick();
			Log.assertThat(collectionListPage.presenceOfAlbumMenu(), "Album menu icon is visible when control is into Project page", "Album menu icon is not visible when control is into Project page",driver);
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 2, description = "TC_018: Verify Album creation.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyAlbumCreation(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_018: Verify Album creation.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			collectionListPage.openProject();
			collectionListPage.menuButtonClick();
			albumPage = collectionListPage.albumButtonClick();
			albumPage.deleteAlbum();
			Log.assertThat(albumPage.clickbtnCreateAlbum(), "Album created successfully", "Album creation failed", driver);
			collectionListPage.logOff();
			
			//Log.assertThat(collectionListPage.presenceOfAlbumMenu(), "Album menu icon is visible when control is into Project page", "Album menu icon is not visible when control is into Project page",driver);
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			//driver.resetApp();
			//driver.closeApp();
			//collectionListPage.logOff();
			//driver.launchApp();
		}
	}
	
	@Test(priority = 3, description = "TC_019: Verify Delete Album.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyAlbumDeletion(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_019: Verify delete Album.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			collectionListPage.openProject();
			collectionListPage.menuButtonClick();
			albumPage = collectionListPage.albumButtonClick();
			albumPage.deleteAlbum();
			albumPage.clickbtnCreateAlbum();
			Log.assertThat(albumPage.deleteAlbum(),"Album successfully deleted", "Album deletion unsuccessful", driver);
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			//driver.resetApp();
			//driver.closeApp();
			//collectionListPage.logOff();
			//driver.launchApp();
		}
	}
	
	@Test(priority = 4, description = "TC_020: Verify Photo upload.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyPhotoUpload(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_020: Verify Photo upload.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			collectionListPage.openProject();
			collectionListPage.menuButtonClick();
			albumPage = collectionListPage.albumButtonClick();
			albumPage.deleteAlbum();
			albumPage.clickbtnCreateAlbum();
			albumPage.openAlbum();
			Log.assertThat(albumPage.uploadPhoto(), "Photo uploaded successfully", "Photo upload unsuccessful", driver);
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			//driver.resetApp();
			//driver.closeApp();
			//collectionListPage.logOff();
			//driver.launchApp();
		}
	}
	
	@Test(priority = 5, description = "TC_021: Verify Share Album.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyAlbumSharing(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_021: Verify Share Album.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			collectionListPage.openProject();
			collectionListPage.menuButtonClick();
			albumPage = collectionListPage.albumButtonClick();
			albumPage.deleteAlbum();
			albumPage.clickbtnCreateAlbum();
			albumPage.openAlbum();
			albumPage.uploadPhoto();
			albumPage.returnAlbumPage();
			Log.assertThat(albumPage.sharingAlbum(), "Album shared successfully", "Album sharing failed", driver);
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			//driver.resetApp();
			//driver.closeApp();
			//collectionListPage.logOff();
			//driver.launchApp();
		}
	}
	
	@Test(priority = 6, description = "TC_022: Verify Share Photo.", enabled = true , retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyPhotoSharing(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_022: Verify Share Photo.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			collectionListPage.openProject();
			collectionListPage.menuButtonClick();
			albumPage = collectionListPage.albumButtonClick();
			albumPage.deleteAlbum();
			albumPage.clickbtnCreateAlbum();
			albumPage.openAlbum();
			albumPage.uploadPhoto();
			Log.assertThat(albumPage.sharingPhoto(), "Photo shared successfully", "Photo sharing failed", driver);
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			//driver.resetApp();
			//driver.closeApp();
			//collectionListPage.logOff();
			//driver.launchApp();
		}
	}
	
	@AfterClass
	public void quit()
	{
		driver.quit();
	}
	
}
